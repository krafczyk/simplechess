//
//  Logging.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Logging.h"

#include <cstdlib>

int Logging::_indentLevel = 0;
bool Logging::_shouldIndent = false;

void Logging::info(const char* fmt, ...)
{
    if (_shouldIndent)
    {
        for (int i = 0; i < _indentLevel; ++i)
        {
            fprintf(stdout, "\t");
        }
        _shouldIndent = false;
    }
    
    va_list args;
    va_start(args, fmt);
    vfprintf(stdout, fmt, args);
    va_end(args);

    size_t length = strlen(fmt);
    if (length > 0 && fmt[length - 1] == '\n')
    {
        _shouldIndent = true;
    }
}

void Logging::warn(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vfprintf(stdout, fmt, args);
    va_end(args);
}

void Logging::error(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
}

void Logging::increaseIndentLevel(void)
{
    fprintf(stdout, "\n");
    ++_indentLevel;
    _shouldIndent = true;
}

void Logging::decreaseIndentLevel(void)
{
    --_indentLevel;
}
