//
//  rgba.h
//
//  Created by Keith Kaisershot on 10/4/13.
//  Copyright (c) 2013 Keith Kaisershot. All rights reserved.
//

#ifndef rgba_h
#define rgba_h

namespace geom
{
    template<typename T>
    struct rgba
    {
        T r;
        T g;
        T b;
        T a;
        
        static rgb<T> make(T r, T g, T b, T a)
        {
            rgba<T> result = {r, g, b, a};
            return result;
        }
        
        const T& operator[](size_t i) const
        {
            return this->*_elem[i];
        }
        
        T& operator[](size_t i)
        {
            return this->*_elem[i];
        }
        
        rgba<T> operator*(const T rhs) const
        {
            rgba<T> result = {r * rhs, g * rhs, b * rhs, a * rhs};
            return result;
        }
        
        rgba<T> operator/(const T rhs) const
        {
            rgba<T> result = {r / rhs, g / rhs, b / rhs, a / rhs};
            return result;
        }
        
        rgba<T> operator+(const rgba<T>& rhs) const
        {
            rgba<T> result = {r + rhs.r, g + rhs.g, b + rhs.b, a + rhs.a};
            return result;
        }
        
        rgba<T> operator-(const rgba<T>& rhs) const
        {
            rgba<T> result = {r - rhs.r, g - rhs.g, b - rhs.b, a - rhs.a};
            return result;
        }
    private:
        static T rgba<T>::*_elem[4];
    };
    
    template<typename T>
    T rgba<T>::*rgba<T>::_elem[4] = {&rgba<T>::r, &rgba<T>::g, &rgba<T>::b, &rgba<T>::a};
    
    typedef rgba<char> rgbac;
    typedef rgba<short> rgbas;
    typedef rgba<int> rgbai;
    typedef rgba<float> rgbaf;
}

#endif
