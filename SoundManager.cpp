//
//  SoundManager.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "SoundManager.h"

#include "Event.h"
#include "Logging.h"
#include "Sound.h"

SoundManager::SoundManager(void)
{
    _inited = true;
    Logging::info("Initializing sound manager... ");
    
    // the sound file is 11.128 Khz mono
    if (Mix_OpenAudio(11128, MIX_DEFAULT_FORMAT, 1, 2048) == -1)
    {
        Logging::error("Could not initialize SDL_mixer: %s\n", Mix_GetError());
        _inited = false;
        shutdown();
    }
    
    Logging::info("done.\n");
}

SoundManager::~SoundManager(void)
{
    shutdown();
}

void SoundManager::shutdown(void)
{
    if (_inited)
    {
        Logging::info("Shutting down sound manager... ");

        Mix_Quit();
        
        Logging::info("done.\n");
        _inited = false;
    }
}

void SoundManager::addSound(Sound& sound, int eventType)
{
    _sounds[eventType] = &sound;
}

bool SoundManager::receiveEvent(Event& event)
{
    // only play the sound if it maps to this event
    if (_sounds.find(event.type()) != _sounds.end())
    {
        _sounds[event.type()]->play();
    }
    
    return true;
}
