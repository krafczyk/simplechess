//
//  rgb.h
//
//  Created by Keith Kaisershot on 10/4/13.
//  Copyright (c) 2013 Keith Kaisershot. All rights reserved.
//

#ifndef rgb_h
#define rgb_h

namespace geom
{
    template<typename T>
    struct rgb
    {
        T r;
        T g;
        T b;
        
        static rgb<T> make(T r, T g, T b)
        {
            rgb<T> result = {r, g, b};
            return result;
        }
        
        const T& operator[](size_t i) const
        {
            return this->*_elem[i];
        }
        
        T& operator[](size_t i)
        {
            return this->*_elem[i];
        }
        
        rgb<T> operator*(const T rhs) const
        {
            rgb<T> result = {r * rhs, g * rhs, b * rhs};
            return result;
        }
        
        rgb<T> operator/(const T rhs) const
        {
            rgb<T> result = {r / rhs, g / rhs, b / rhs};
            return result;
        }
        
        rgb<T> operator+(const rgb<T>& rhs) const
        {
            rgb<T> result = {r + rhs.r, g + rhs.g, b + rhs.b};
            return result;
        }
        
        rgb<T> operator-(const rgb<T>& rhs) const
        {
            rgb<T> result = {r - rhs.r, g - rhs.g, b - rhs.b};
            return result;
        }
    private:
        static T rgb<T>::*_elem[3];
    };
    
    template<typename T>
    T rgb<T>::*rgb<T>::_elem[3] = {&rgb<T>::r, &rgb<T>::g, &rgb<T>::b};
    
    typedef rgb<char> rgbc;
    typedef rgb<short> rgbs;
    typedef rgb<int> rgbi;
    typedef rgb<float> rgbf;
}

#endif
