//
//  HUD.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/12/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "HUD.h"

#include <cassert>

#include "Colors.h"
#include "Event.h"
#include "Logging.h"
#include "Piece.h"
#include "ScoreEvent.h"
#include "Screen.h"
#include "Shader.h"
#include "TakeTurnEvent.h"
#include "vec2.h"

using namespace math;

static const vec2f buttonVerts[] =
{
    {0.0f, 0.0f},
    {0.0f, 1.0f},
    {1.0f, 1.0f},
    {1.0f, 0.0f}
};

static const vec2f arrowTris[] =
{
    {0.0f, 0.0f},
    {-1.0f, 1.0f},
    {0.0f, 1.0f},
    {0.0f, 1.0f},
    {1.0f, 1.0f},
    {0.0f, 0.0f},
    {-0.5f, 1.0f},
    {-0.5f, 2.0f},
    {0.5f, 1.0f},
    {0.5f, 1.0f},
    {-0.5f, 2.0f},
    {0.5f, 2.0f}
};

static const int fontSize = 20;

static const float buttonScale = 1.05f;
static const float buttonBorderScale = 1.3f;

static const float arrowScale = 10.0f;
static const float arrowYOffsetScale = 1.75f;

HUD::HUD(void)
{
    _inited = true;
    Logging::info("Initializing HUD... ");
    Logging::increaseIndentLevel();

    _blackBadge = new Badge(fontSize, Colors::white);
    _whiteBadge = new Badge(fontSize, Colors::white);
    _buttonBadge = new Badge(fontSize, Colors::white);
    
    _colorShader = new Shader("SimpleColored.vs", "SimpleColored.fs");
    
    glGenBuffers(1, &_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(buttonVerts), buttonVerts, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &_arrow);
    glBindBuffer(GL_ARRAY_BUFFER, _arrow);
    glBufferData(GL_ARRAY_BUFFER, sizeof(arrowTris), arrowTris, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    _buttonBadge->updateText("Reset");
    
    Logging::decreaseIndentLevel();
    Logging::info("done.\n");
}

HUD::~HUD(void)
{
    shutdown();
}

void HUD::shutdown(void)
{
    if (_inited)
    {
        Logging::info("Shutting down HUD... ");
        
        glDeleteBuffers(1, &_vbo);
        delete _colorShader;
        
        delete _blackBadge;
        delete _whiteBadge;
        delete _buttonBadge;
        
        Logging::info("done.\n");
        _inited = false;
    }
}

bool HUD::click(Screen& screen, pt2f coords)
{
    pt2f screenCoords = {(coords.x + 1.0f) * screen.getWidth() / 2.0f, (coords.y - 1.0f) * screen.getHeight() / -2.0f};
    // we only have one button, so just check for its bounding box
    if (screenCoords.x >= (screen.getWidth() - _buttonBadge->getWidth() * buttonBorderScale) / 2.0f &&
        screenCoords.x < (screen.getWidth() + _buttonBadge->getWidth() * buttonBorderScale) / 2.0f &&
        screenCoords.y >= 5.0f - (_buttonBadge->getHeight() * (buttonBorderScale - 1.0f) / 2.0f) &&
        screenCoords.y < 5.0f + (_buttonBadge->getHeight() * buttonBorderScale))
    {
        return true;
    }
    
    return false;
}

void HUD::updateScore(int blackScore, int whiteScore)
{
    // scores can't get higher than 3, so a char[9] is fine here
    // but it's a char[10] just in case the game is updated later to support a full chess set
    char text[10];
    sprintf(text, "Black: %i", blackScore);
    _blackBadge->updateText(text);
    sprintf(text, "White: %i", whiteScore);
    _whiteBadge->updateText(text);
}

void HUD::draw(const Screen& screen) const
{
    // we don't need the depth buffer here since we control the draw order
    
    // use screen coordinates for HUD elements
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0f, screen.getWidth(), screen.getHeight(), 0.0f, -1.0f, 1.0f);
    glMatrixMode(GL_MODELVIEW);
        
    // draw the black score on the left side of the screen
    glLoadIdentity();
    glTranslatef(7.0f, 5.0f, 0.0f);
    _blackBadge->draw(screen);
        
    // draw the white score on the right side of the screen
    glLoadIdentity();
    glTranslatef(screen.getWidth() - _whiteBadge->getWidth() - 7.0f, 5.0f, 0.0f);
    _whiteBadge->draw(screen);

    _colorShader->use();

    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

    // draw a black border around the button
    glUniform3fv(_colorShader->getUniform("color"), 1, (GLfloat*)&Colors::black);
    glLoadIdentity();
    glTranslatef((screen.getWidth() - _buttonBadge->getWidth() * buttonBorderScale) / 2.0f,
                 5.0f - (_buttonBadge->getHeight() * (buttonBorderScale - 1.0f) / 2.0f),
                 0.0f);
    glScalef(_buttonBadge->getWidth() * buttonBorderScale, _buttonBadge->getHeight() * buttonBorderScale, 1.0f);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    // fill in the button with a flat color
    glUniform3fv(_colorShader->getUniform("color"), 1, (GLfloat*)&Colors::gray);
    glLoadIdentity();
    glTranslatef((screen.getWidth() - _buttonBadge->getWidth() * buttonScale) / 2.0f,
                 5.0f - (_buttonBadge->getHeight() * (buttonScale - 1.0f) / 2.0f),
                 0.0f);
    glScalef(_buttonBadge->getWidth() * buttonScale, _buttonBadge->getHeight() * buttonScale, 1.0f);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    // draw the button text centered in the button, centered in the center top of the screen
    glLoadIdentity();
    glTranslatef((screen.getWidth() - _buttonBadge->getWidth()) / 2.0f, 5.0f, 0.0f);
    _buttonBadge->draw(screen);
    
    _colorShader->use();

    // draw the arrow indicating which player's turn
    glLoadIdentity();
    if (_arrowXOffset < 0.0f)
    {
        glTranslatef(screen.getWidth() + _arrowXOffset, fontSize * arrowYOffsetScale, 0.0f);        
    }
    else
    {
        glTranslatef(_arrowXOffset, fontSize * arrowYOffsetScale, 0.0f);
    }
    glScalef(arrowScale, arrowScale, 1.0f);
    
    glUniform3fv(_colorShader->getUniform("color"), 1, (GLfloat*)&Colors::white);

    glBindBuffer(GL_ARRAY_BUFFER, _arrow);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glDrawArrays(GL_TRIANGLES, 0, 12);
    
    glDisableVertexAttribArray(0);
    
    Shader::useDefault();
    glDisable(GL_BLEND);
}

bool HUD::receiveEvent(Event &event)
{
    switch (event.type())
    {
        case 'scor':
            {
                ScoreEvent* scoreEvent = (ScoreEvent*)&event;
                updateScore(scoreEvent->blackScore(), scoreEvent->whiteScore());
            }
            break;
        case 'turn':
            {
                TakeTurnEvent* takeTurnEvent = (TakeTurnEvent*)&event;
                _arrowXOffset = takeTurnEvent->currentPlayer() == Piece::BLACK ? 40 : -40;
            }
            break;
        default:
            break;
    }
    
    return true;
}
