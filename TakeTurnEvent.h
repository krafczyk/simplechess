//
//  TakeTurnEvent.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_TakeTurnEvent_h
#define SimpleChess_TakeTurnEvent_h

#include "Event.h"

class TakeTurnEvent : public Event
{
public:
    TakeTurnEvent(int currentPlayer) 
    : Event('turn')
    , _currentPlayer(currentPlayer) {};
    ~TakeTurnEvent(void) {};
    
    int currentPlayer(void) const { return _currentPlayer; };
    
private:
    int _currentPlayer;
    
};

#endif
