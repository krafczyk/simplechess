//
//  HUD.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/12/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_HUD_h
#define SimpleChess_HUD_h

#include "GLEW/glew.h"
#include "SDL2_ttf/SDL_ttf.h"

#include "Badge.h"
#include "Drawable.h"
#include "ReceivesEvents.h"
#include "vec2.h"

class Shader;

using namespace math;

class HUD : public Drawable, public ReceivesEvents
{
public:
    HUD(void);
    ~HUD(void);
    
    bool isInited(void) const { return _inited; };
    
    // coords are in ndc space
    bool click(Screen& screen, pt2f coords);
    
    void draw(const Screen& screen) const;
    
    bool receiveEvent(Event& event);
    
private:
    bool _inited;
    GLuint _vbo;
        
    Badge* _blackBadge;
    Badge* _whiteBadge;
    
    Badge* _buttonBadge;
    vec2f _buttonPos;
    
    GLuint _arrow;
    int _arrowXOffset;
    
    Shader* _colorShader;
    
    void shutdown(void);
    
    // width and height are in screen coords
    // texWidth and texHeight are texture coords
    void updateTexture(GLuint& texture, const char* text, int& width, int& height, GLfloat& texWidth, GLfloat& texHeight);
    void updateScore(int blackScore, int whiteScore);
    
};

#endif
