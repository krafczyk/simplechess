//
//  Badge.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Badge.h"

#include "Logging.h"
#include "Shader.h"
#include "vec2.h"

using namespace math;

static const vec2f vertices[] =
{
    {0.0f, 0.0f},
    {0.0f, 1.0f},
    {1.0f, 1.0f},
    {1.0f, 0.0f}
};

bool Badge::_inited = false;
int Badge::_glRefCount = 0;
Shader* Badge::_shader = NULL;
GLuint Badge::_vbo;

Badge::Badge(int fontSize, rgbf color)
: _fontSize(fontSize)
, _color(color)
, _alphaFactor(1.0f)
, _texture(0)
{
    if (_glRefCount == 0)
    {
        _inited = true;
        Logging::info("Initializing text... ");

        if (TTF_Init() == -1)
        {
            Logging::error("Could not initialize SDL_ttf: %s\n", TTF_GetError());
            _inited = false;
            return;
        }
        
        _shader = new Shader("SimpleTextured.vs", "SimpleTextured.fs");
        
        glGenBuffers(1, &_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, _vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) * 2, NULL, GL_DYNAMIC_DRAW);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        Logging::info("done.\n");
    }
    _glRefCount++;
    
    _font = TTF_OpenFont("SourceSansPro-Regular.ttf", fontSize);
    if (_font == NULL)
    {
        Logging::error("Could not load font: %s\n", TTF_GetError());
    }
}

Badge::~Badge(void)
{
    if (_texture != 0)
    {
        glDeleteTextures(1, &_texture);
    }
    
    _glRefCount--;
    if (_glRefCount == 0)
    {
        shutdown();
    }
}

void Badge::shutdown(void)
{
    if (_inited)
    {
        Logging::info("Shutting down text... ");
        
        glDeleteBuffers(1, &_vbo);
        delete _shader;
        if (_font != NULL)
        {
            TTF_CloseFont(_font);
            _font = NULL;
        }
        TTF_Quit();
        
        Logging::info("done.\n");
        _inited = false;
    }
}

// from http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
static int nextPowerOfTwo(int n)
{
    n--;
    n |= n >> 1;
    n |= n >> 2;
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;
    n++;
    
    return n;
}

void Badge::updateText(const char* text)
{    
    if (_font != NULL)
    {
        SDL_Color color = {_color.r * 255, _color.g * 255, _color.b * 255, 255};
        SDL_Surface* surface = TTF_RenderText_Blended(_font, text, color);
        if (surface == NULL)
        {
            Logging::error("Could not render text surface: %s\n", TTF_GetError());
            return;
        }
        
        // borrowed from glfont.c in SDL_ttf
        int w, h;
        
        w = nextPowerOfTwo(surface->w);
        h = nextPowerOfTwo(surface->h);
        _maxU = (GLfloat)surface->w / w;
        _maxV = (GLfloat)surface->h / h;
        
        SDL_Surface* image = SDL_CreateRGBSurface(
            SDL_SWSURFACE,
            w, h,
            32,
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
            0x000000FF, 
            0x0000FF00, 
            0x00FF0000, 
            0xFF000000
#else
            0xFF000000,
            0x00FF0000, 
            0x0000FF00, 
            0x000000FF
#endif
        );
        if (image == NULL)
        {
            Logging::error("Could not create intermediate surface from rendered text: %s\n", SDL_GetError());
            return;
        }
        
        // save the alpha blending attributes
        Uint8 savedAlpha;
        SDL_BlendMode savedMode;
        SDL_GetSurfaceAlphaMod(surface, &savedAlpha);
        SDL_SetSurfaceAlphaMod(surface, 0xFF);
        SDL_GetSurfaceBlendMode(surface, &savedMode);
        SDL_SetSurfaceBlendMode(surface, SDL_BLENDMODE_NONE);
        
        // copy the surface into the GL texture image
        SDL_Rect rect = {0, 0, surface->w, surface->h};
        SDL_BlitSurface(surface, &rect, image, &rect);
        
        // restore the alpha blending attributes
        SDL_SetSurfaceAlphaMod(surface, savedAlpha);
        SDL_SetSurfaceBlendMode(surface, savedMode);
        
        if (_texture == 0)
        {
            glGenTextures(1, &_texture);
        }
        glBindTexture(GL_TEXTURE_2D, _texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->pixels);
        
        _width = surface->w;
        _height = surface->h;
        
        SDL_FreeSurface(image);
    }
}

void Badge::setAlphaFactor(float alphaFactor)
{
    _alphaFactor = alphaFactor;
}

void Badge::draw(const Screen& screen) const
{
    const vec2f texCoords[] =
    {
        {0.0f,  0.0f},
        {0.0f,  _maxV},
        {_maxU, _maxV},
        {_maxU, 0.0f}
    };
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    _shader->use();
    
    glScalef(_width, _height, 1.0f);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _texture);
    glUniform1i(_shader->getUniform("texture"), 0);
    glUniform1f(_shader->getUniform("alphaFactor"), _alphaFactor);
    
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    // update the tex coords with the results from updateText
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices), sizeof(texCoords), texCoords);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)32);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    
    Shader::useDefault();
}
