//
//  Tile.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Tile.h"

#include <cassert>

#include "Board.h"
#include "Colors.h"
#include "Piece.h"
#include "Shader.h"
#include "vec3.h"

using namespace math;

static const vec3f vertices[] =
{
    {0.0f, 0.0f, 0.0f},
    {0.0f, 1.0f, 0.0f},
    {1.0f, 1.0f, 0.0f},
    {1.0f, 0.0f, 0.0f}
};

static const int hilitePeriod = 2000;   // two seconds fade in/out

int Tile::_glRefCount = 0;
Shader* Tile::_shader;
GLuint Tile::_vbo;

Tile::Tile(Color color)
: _color(color)
, _piece(NULL)
{
    if (_glRefCount == 0)
    {
        _shader = new Shader("SimpleColored.vs", "SimpleColored.fs");

        glGenBuffers(1, &_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, _vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    _glRefCount++;
    
    _hiliteRGB = getRGB();
    setHilite(false);
}

Tile::~Tile(void)
{
    _glRefCount--;
    if (_glRefCount == 0)
    {
        glDeleteBuffers(1, &_vbo);
        delete _shader;
    }
}

void Tile::draw(const Screen& screen) const
{
    _shader->use();
    
    glUniform3fv(_shader->getUniform("color"), 1, (GLfloat*)&_rgb);

    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glDisableVertexAttribArray(0);
    
    Shader::useDefault();
}

rgbf Tile::getRGB(void) const
{
    // the "black" tiles are actually dark gray
    // so that the black pieces can be seen against black tiles
    return _color == BLACK ? Colors::darkGray : Colors::white;
}

void Tile::setHilite(bool hilite)
{
    // setting hilite when hilite is already active
    // will reset the hilite cycle
    _hiliting = hilite;
    _hiliteTime = 0;
    _rgb = getRGB();
}

void Tile::setHiliteColor(rgbf color)
{
    _hiliteRGB = color;
}

void Tile::update(int deltaMS)
{
    _hiliteTime = (_hiliteTime + deltaMS) % hilitePeriod;
    if (_hiliting)
    {
        // use a dampened sine curve for a smooth fade in/out that isn't too bright
        float t = sinf((float)_hiliteTime * M_PI / (float)hilitePeriod) * 0.75f;
        _rgb = _hiliteRGB * t + getRGB() * (1.0f - t);
    }
}
