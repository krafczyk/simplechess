//
//  Board.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Board.h"

#include <cassert>

#include "Badge.h"
#include "CaptureEvent.h"
#include "Colors.h"
#include "EventManager.h"
#include "Logging.h"
#include "mat4.h"
#include "MouseMoveEvent.h"
#include "Piece.h"
#include "Screen.h"
#include "Shader.h"
#include "Tile.h"
#include "vec3.h"

static const vec3f vertices[] =
{
    {-0.5f, -0.5f, 0.0f},
    {-0.5f, 0.5f, 0.0f},
    {0.5f, 0.5f, 0.0f},
    {0.5f, -0.5f, 0.0f}
};

static const float backBoardScale = 1.05f;
static const float yDistFromBoard = 4.0f;
static const float zDistFromBoard = 12.5f;

static const int pieceAnimatePeriod = 1500;
static const int vfxAnimatePeriod = 1000;

static const int vfxFontSize = 15;
static const int vfxMaxYOffset = 20;

Board::Board(int width, int height, EventManager& eventManager)
: _eventManager(eventManager)
, _width(width)
, _height(height)
, _animatingPiece(false)
, _animatingVFX(false)
{
    _shader = new Shader("SimpleColored.vs", "SimpleColored.fs");
    
    glGenBuffers(1, &_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // create checkerboard pattern
    for (int i = 0; i < _width; ++i)
    {
        for (int k = 0; k < _height; ++k)
        {
            _tiles.push_back(new Tile((i % 2 ^ k % 2) ? Tile::BLACK : Tile::WHITE));
        }
    }
    
    _vfx = new Badge(vfxFontSize, Colors::black);
    _vfx->updateText("Captured!");
}

Board::~Board(void)
{
    for (std::vector<Tile*>::iterator tileIter = _tiles.begin(); tileIter != _tiles.end(); tileIter++)
    {
        delete *tileIter;
    }
    
    delete _vfx;
}

bool Board::isTileOpen(int x, int y) const
{
    // a tile is considered open if it has valid coordinates
    // and is unoccupied by a piece
    Tile* tile = getTile(x, y);
    return tile != NULL && tile->getPiece() == NULL;
}

Tile* Board::getTile(int x, int y) const
{
    if (x < 0 || x >= _width ||
        y < 0 || y >= _height)
    {
        return NULL;
    }
    
    return _tiles[y * _width + x];
}

Tile* Board::findTile(Screen& screen, vec3f ray, pt2i* tileCoords) const
{
    // do ray/plane intersection with the board
    // then derive tile from coordinates of hit point
    vec3f n = {0.0f, 1.0f, 0.0f};
    vec3f p0 = {0.0f, -yDistFromBoard, 0.0f};
    float denom = ray.dot(n);
    if (denom >= 0.0f)
    {
        return NULL;
    }
    
    float t = p0.dot(n) / denom;
    vec3f hitPt = ray * t;
    // the board is offset from the origin by (4, zDistFromBoard)
    // so it appears centered
    vec2i hitCoords = {(int)(hitPt.x + 4.0f), (int)(hitPt.z + zDistFromBoard)};
    Tile* tile = getTile(hitCoords.x, hitCoords.y);
    if (tile != NULL && tileCoords != NULL)
    {
        *tileCoords = pt2i::make(hitCoords.x, hitCoords.y);
    }
    return tile;
}

void Board::placePiece(Piece& piece, int x, int y, bool animate)
{
    assert(x >= 0 && x < _width &&
           y >= 0 && y < _height);
    
    // if we're animating then defer actually moving the piece
    // until the animation is done
    // instead just set up the animation
    if (animate)
    {
        piece._destX = x;
        piece._destY = y;
        
        _movingPiece = &piece;
        _destTile = getTile(piece._destX, piece._destY);
        _pieceAnimateTime = 0;
        _animatingPiece = true;
    }
    // otherwise just instantly place the piece
    else
    {
        Tile* tile = getTile(x, y);
        tile->assignPiece(&piece);
        piece._x = x;
        piece._y = y;
        _occupiedTiles[&piece] = tile;
    }
}

void Board::removePiece(Piece& piece)
{
    assert(_occupiedTiles.find(&piece) != _occupiedTiles.end());
    
    Tile* tile = _occupiedTiles[&piece];
    tile->assignPiece(NULL);
    _occupiedTiles.erase(&piece);
}

void Board::clear(void)
{
    for (OccupiedTileContainer::iterator tileIter = _occupiedTiles.begin(); tileIter != _occupiedTiles.end(); tileIter++)
    {
        (*tileIter).second->assignPiece(NULL);
    }
}

void Board::draw(const Screen& screen) const
{
    _shader->use();
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMultMatrixf((GLfloat*)&screen.getPersp());
    glMatrixMode(GL_MODELVIEW);
    
    // render dark gray backboard
    // we don't need the back buffer enabled here
    // since we know it's the farthestmost object drawn
    glLoadIdentity();
    glTranslatef(0.0f, -yDistFromBoard, -zDistFromBoard + _height / 2.0f);
    glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
    glScalef(_width * backBoardScale, _height * backBoardScale, 1.0f);
    
    glUniform3fv(_shader->getUniform("color"), 1, (GLfloat*)&Colors::gray);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glDisableVertexAttribArray(0);
    
    Shader::useDefault();

    // draw each tile and piece from front to back
    glEnable(GL_DEPTH_TEST);
    for (int y = 0; y < _height; ++y)
    {
        for (int x = 0; x < _width; ++x)
        {
            Tile* tile = getTile(x, y);
            
            glLoadIdentity();
            glTranslatef(-4.0f + x, -yDistFromBoard, -zDistFromBoard + y);
            glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
            
            tile->draw(screen);
            
            // draw the piece on top of the tile
            const Piece* piece = tile->getPiece();
            if (piece != NULL)
            {
                if (_animatingPiece)
                {
                    glTranslatef(piece->_offsetX, piece->_offsetY, 0.0f);
                }
                piece->draw(screen);
            }
        }
    }
    glDisable(GL_DEPTH_TEST);
    
    // draw some floating fading text above the piece that was just captured
    if (_animatingVFX)
    {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glMultMatrixf((GLfloat*)&screen.getOrtho());
        glMatrixMode(GL_MODELVIEW);
        
        glLoadIdentity();
        mat4f mvpMat = screen.getPersp() * mat4f::trans(vec3f::make(-4.0f, -yDistFromBoard, -zDistFromBoard));
    
        pt3f vfxScreenBase = screen.project(_vfxWorldBase, mvpMat);
        vfxScreenBase.x = (vfxScreenBase.x + 1.0f) * screen.getWidth() / 2.0f;
        vfxScreenBase.y = (vfxScreenBase.y - 1.0f) * screen.getHeight() / -2.0f;
        glTranslatef(vfxScreenBase.x - _vfx->getWidth() / 2.0f, vfxScreenBase.y + _vfxYOffset, 0.0f);
        
        _vfx->draw(screen);
    }
}

void Board::update(int deltaMS)
{
    if (_animatingPiece)
    {
        _pieceAnimateTime += deltaMS;
        // if we're done animating a piece
        // then remove it from its previous location
        // and put it in its new location
        // removing any piece that might be in the new location
        if (_pieceAnimateTime > pieceAnimatePeriod)
        {
            _animatingPiece = false;
            Piece* destPiece = _destTile->getPiece();
            if (destPiece != NULL)
            {
                removePiece(*destPiece);
                // setup the captured effects
                _vfxWorldBase = pt3f::make(destPiece->getX() + 0.5f, 2.0f, destPiece->getY() + 0.5f);
                _eventManager.postEvent(*new CaptureEvent());
                
            }
            removePiece(*_movingPiece);
            placePiece(*_movingPiece, _movingPiece->_destX, _movingPiece->_destY);
            _movingPiece->_offsetX = 0.0f;
            _movingPiece->_offsetY = 0.0f;
        }
        else
        {
            float t = (float)_pieceAnimateTime / (float)pieceAnimatePeriod;
            _movingPiece->_offsetX = (_movingPiece->_destX - _movingPiece->_x) * t;
            _movingPiece->_offsetY = (_movingPiece->_destY - _movingPiece->_y) * t;
        }
    }
    
    if (_animatingVFX)
    {
        _vfxAnimateTime += deltaMS;
        if (_vfxAnimateTime > vfxAnimatePeriod)
        {
            _animatingVFX = false;
        }
        else
        {
            float t = (float)_vfxAnimateTime / (float)vfxAnimatePeriod;
            _vfxYOffset = (float)vfxMaxYOffset * -t;
            _vfx->setAlphaFactor(1.0f - t);
        }
    }
}

bool Board::receiveEvent(Event& event)
{
    if (event.type() == 'capt')
    {
        // start the captured effects
        _vfxYOffset = 0;
        _vfxAnimateTime = 0;
        _animatingVFX = true;        
    }
    
    return true;
}
