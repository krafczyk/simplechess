//
//  Queen.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Queen.h"

#include "Board.h"
#include "Piece.h"
#include "Tile.h"

void Queen::getValidMoves(Board& board, std::set<Tile*>& moveList) const
{
    moveList.clear();
    
    // "A Queen can move an unlimited number of diagonal, vertical, or horizontal tiles unless blocked by another piece"
    const vec2i searchDirections[] =
    {
        {-1, -1},
        {0, -1},
        {1, -1},
        {1, 0},
        {1, 1},
        {0, 1},
        {-1, 1},
        {-1, 0}
    };
    
    for (int i = 0; i < 8; ++i)
    {
        int thisX = getX() + searchDirections[i].x;
        int thisY = getY() + searchDirections[i].y;
        
        while (Tile* thisTile = board.getTile(thisX, thisY))
        {
            const Piece* piece = thisTile->getPiece();
            if (piece != NULL)
            {
                if (piece->getColor() == getColor())
                {
                    // we can't move to a space occupied by our own color
                    break;
                }
                else
                {
                    // we can take an opponent but that terminates the move
                    moveList.insert(thisTile);
                    break;
                }
            }
            else
            {
                moveList.insert(thisTile);
            }
            
            thisX += searchDirections[i].x;
            thisY += searchDirections[i].y;
        }
    }
}
