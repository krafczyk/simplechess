//
//  main.m
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/12/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Game.h"

int main(int argc, char *argv[])
{
    Game* game = new Game();
    while (game->isRunning())
    {
        game->processFrame();
    }
    delete game;
}
