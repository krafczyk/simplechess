//
//  mat2.h
//
//  Created by Keith Kaisershot on 10/4/13.
//  Copyright (c) 2013 Keith Kaisershot. All rights reserved.
//

#ifndef mat2_h
#define mat2_h

#include <math.h>

#include "pt2.h"
#include "vec2.h"

namespace math
{
    template<typename T>
    struct mat2
    {
        T m00;
        T m10;
        T m01;
        T m11;
        
        static mat2<T> make(
            T m00, T m01,
            T m10, T m11)
        {
            mat2<T> result = {
                m00, m10,
                m01, m11
            };
            return result;
        }
        
        const T& operator[](const size_t i) const
        {
            return this->*_elem[i];
        }
        
        T& operator[](const size_t i)
        {
            return this->*_elem[i];
        }
        
        const T& operator()(const size_t row, const size_t col) const
        {
            return this->*_elem[row + (col * 2)];
        }
        
        T& operator()(const size_t row, const size_t col)
        {
            return this->*_elem[row + (col * 2)];
        }
        
        mat2<T> operator+(const mat2<T>& rhs) const
        {
            mat2<T> result = {
                m00 + rhs.m00, m10 + rhs.m10,
                m01 + rhs.m01, m11 + rhs.m11
            };
            return result;
        }
        
        mat2<T> operator-(const mat2<T>& rhs) const
        {
            mat2<T> result = {
                m00 - rhs.m00, m10 - rhs.m10,
                m01 - rhs.m01, m11 - rhs.m11
            };
            return result;
        }
        
        mat2<T> operator*(const T rhs) const
        {
            mat2<T> result = {
                m00 * rhs, m10 * rhs,
                m01 * rhs, m11 * rhs
            };
            return result;
        }
        
        mat2<T> operator/(const T rhs) const
        {
            mat2<T> result = {
                m00 / rhs, m10 / rhs,
                m01 / rhs, m11 / rhs
            };
            return result;
        }
        
        pt2<T> operator*(const pt2<T>& rhs) const
        {
            pt2<T> result = {
                (m00 * rhs.x) + (m01 * rhs.y),
                (m10 * rhs.x) + (m11 * rhs.y)
            };
            return result;
        }
        
        vec2<T> operator*(const vec2<T>& rhs) const
        {
            vec2<T> result = {
                (m00 * rhs.x) + (m01 * rhs.y),
                (m10 * rhs.x) + (m11 * rhs.y)
            };
            return result;
        }
        
        float det() const
        {
            return (m00 * m11) - (m01 * m10);
        }
        
        T minor(const size_t row, const size_t col) const
        {
            return (*this)(1 - row, 1 - col);
        }
        
        float cofactor(const size_t row, const size_t col) const
        {
            return minor(row, col) * ((row + col % 2) * -2 + 1);
        }
        
        mat2<T> adj() const
        {
            mat2<T> result = {
                m11, -m01,
                -m01, m00
            };
            return result;
        }
        
        mat2<T> inv() const
        {
            return adj() / det();
        }
        
        mat2<T> transpose() const
        {
            mat2<T> result = {
                m00, m01,
                m10, m11
            };
            return result;
        }
        
        static mat2<T> ident()
        {
            mat2<T> result = {
                1, 0,
                0, 1
            };
            return result;
        }
        
        static mat2<T> rot(const float radians)
        {
            float c = cosf(radians);
            float s = sinf(radians);
            mat2<T> result = {
                c, -s,
                s, c
            };
            return result;
        }
        
        static mat2<T> scale(const float scale)
        {
            mat2<T> result = {
                scale, 0,
                0, 1
            };
            return result;
        }
    private:
        static T mat2<T>::*_elem[2 * 2];
    };
    
    template<typename T>
    T mat2<T>::*mat2<T>::_elem[2 * 2] = {
        &mat2<T>::m00, &mat2<T>::m10,
        &mat2<T>::m01, &mat2<T>::m11
    };
    
    typedef mat2<char> mat2c;
    typedef mat2<short> mat2s;
    typedef mat2<int> mat2i;
    typedef mat2<float> mat2f;
}

#endif
