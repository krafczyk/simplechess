//
//  MouseDownEvent.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_MouseDownEvent_h
#define SimpleChess_MouseDownEvent_h

#include "MouseEvent.h"

class MouseDownEvent : public MouseEvent
{
public:
    MouseDownEvent(float x, float y) 
    : MouseEvent('down', x, y) {};
    ~MouseDownEvent(void) {};
    
};

#endif
