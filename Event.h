//
//  Event.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/12/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Event_h
#define SimpleChess_Event_h

class Event
{
public:
    Event(int type) : _type(type) {};
    virtual ~Event(void) {};
    
    int type(void) const { return _type; };
    
private:
    int _type;
    
    /* events can be casted to derived class types containing additional data
     based on the value of type
     */
};

#endif
