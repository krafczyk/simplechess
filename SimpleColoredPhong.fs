#version 120

const vec4 MAT_SPEC = vec4(0.7, 0.7, 0.7, 1.0);
const float MAT_SHINY = 0.25 * 128.0f;
const vec4 LIGHT_POS = vec4(0.0, -12.5, 0.0, 0.0);
const vec4 LIGHT_AMB = vec4(0.1, 0.1, 0.1, 1.0);
const vec4 LIGHT_DIFF = vec4(1.0, 1.0, 1.0, 1.0);
const vec4 LIGHT_SPEC = vec4(1.0, 1.0, 1.0, 1.0);

varying vec3 P;
varying vec3 N;
varying vec3 V;
uniform vec3 color;

void main()
{
    vec4 outColor = vec4(0.0);
    vec3 L = (gl_ModelViewMatrix * LIGHT_POS).xyz;
    L = normalize(-L);
    float NdotL = dot(N, L);
    outColor += LIGHT_AMB + (vec4(color, 1.0) * LIGHT_DIFF * max(NdotL, 0));
    if (NdotL > 0)
    {
        outColor += MAT_SPEC * LIGHT_SPEC * pow(max(dot((2 * NdotL * N - L), V), 0), MAT_SHINY);
    }
    
	gl_FragColor = outColor;
}
