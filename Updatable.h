//
//  Updatable.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Updatable_h
#define SimpleChess_Updatable_h

class Updatable
{
public:
    virtual ~Updatable(void) {};
    
    // deltaMS is in milliseconds
    virtual void update(int deltaMS) = 0;
    
};

#endif
