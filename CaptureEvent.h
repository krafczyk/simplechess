//
//  CaptureEvent.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_CaptureEvent_h
#define SimpleChess_CaptureEvent_h

#include "Event.h"

class CaptureEvent : public Event
{
public:
    CaptureEvent(void) : Event('capt') {};
    ~CaptureEvent(void) {};
    
};

#endif
