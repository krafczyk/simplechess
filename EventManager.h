//
//  EventManager.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/12/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_EventManager_h
#define SimpleChess_EventManager_h

#include <set>
#include <vector>

class Event;
class ReceivesEvents;

class EventManager
{
public:
    void registerReceiver(ReceivesEvents& receiver);
    void unregisterReceiver(ReceivesEvents& receiver);
    
    // events become owned by the manager!
    // so make sure to allocate the event, then post it
    void postEvent(Event& event);
    
    void flushEvents(void);
    
private:
    typedef std::set<ReceivesEvents*> ReceiverContainer;
    typedef std::vector<Event*> EventContainer;
    
    ReceiverContainer _receivers;
    EventContainer _events;
    
};

#endif
