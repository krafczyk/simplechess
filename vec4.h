//
//  vec4.h
//
//  Created by Keith Kaisershot on 10/3/13.
//  Copyright (c) 2013 Keith Kaisershot. All rights reserved.
//

#ifndef vec4_h
#define vec4_h

#include <math.h>

#include "pt4.h"

namespace math
{
    template<typename T>
    struct vec4
    {
        T x;
        T y;
        T z;
        T w;
        
        static vec4<T> make(T x, T y, T z, T w)
        {
            vec4<T> result = {x, y, z, w};
            return result;
        }
        
        const T& operator[](size_t i) const
        {
            return this->*_elem[i];
        }
        
        T& operator[](size_t i)
        {
            return this->*_elem[i];
        }
        
        vec4<T> operator-() const
        {
            vec4<T> result = {-x, -y, -z, -w};
            return result;
        }
        
        void neg()
        {
            x = -x;
            y = -y;
            z = -z;
            w = -w;
        }
        
        vec4<T> operator+(const vec4<T>& rhs) const
        {
            vec4<T> result = {x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w};
            return result;
        }
        
        vec4<T> operator-(const vec4<T>& rhs) const
        {
            vec4<T> result = {x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w};
            return result;
        }
        
        vec4<T> operator*(const T rhs) const
        {
            vec4<T> result = {x * rhs, y * rhs, z * rhs, w * rhs};
            return result;
        }
        
        vec4<T> operator/(const T rhs) const
        {
            vec4<T> result = {x / rhs, y / rhs, z / rhs, w / rhs};
            return result;
        }
        
        pt4<T> operator+(const pt4<T>& rhs) const
        {
            pt4<T> result = {x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w};
            return result;
        }
        
        pt4<T> operator-(const pt4<T>& rhs) const
        {
            pt4<T> result = {x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w};
            return result;
        }
        
        float dot(const vec4<T>& rhs) const
        {
            return (x * rhs.x) + (y * rhs.y) + (z * rhs.z) + (w * rhs.w);
        }
        
        float lenSq() const
        {
            return dot(*this);
        }
        
        float len() const
        {
            return sqrtf(lenSq());
        }
        
        vec4<T> normalized() const
        {
            return *this / len();
        }
        
        void normalize()
        {
            *this /= len();
        }
        
        vec4<T> projOn(const vec4<T>& rhs) const
        {
            vec4<T> result = (dot(rhs) / rhs.lenSq()) * rhs;
            return result;
        }
        
        vec3<T> cross(const vec3<T>& rhs) const
        {
            vec3<T> result = {
                (y * rhs.z) - (z * rhs.y),
                (z * rhs.x) - (x * rhs.z),
                (x * rhs.y) - (y * rhs.x),
                1
            };
            return result;
        }
    private:
        static T vec4<T>::*_elem[4];
    };
    
    template<typename T>
    T vec4<T>::*vec4<T>::_elem[4] = {&vec4<T>::x, &vec4<T>::y, &vec4<T>::z, &vec4<T>::w};
    
    typedef vec4<char> vec4c;
    typedef vec4<short> vec4s;
    typedef vec4<int> vec4i;
    typedef vec4<float> vec4f;
}

#endif
