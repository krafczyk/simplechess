//
//  Shader.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Shader.h"

#include "Logging.h"

Shader::Shader(const char* vertexPath, const char* fragmentPath)
: _program(0)
{    
    const char* vertexShader;
    const char* fragmentShader;
    GLuint vertexID, fragmentID;
    GLint result = GL_FALSE, infoLogLength;
    
    // load shaders from file
    vertexShader = readShaderFile(vertexPath);
    fragmentShader = readShaderFile(fragmentPath);
    
    // compile vertex shader
    vertexID = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexID, 1, &vertexShader, NULL);
    glCompileShader(vertexID);
    glGetShaderiv(vertexID, GL_COMPILE_STATUS, &result);
    glGetShaderiv(vertexID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (result != GL_TRUE && infoLogLength > 0)
    {
        char errorMsg[infoLogLength];
        glGetShaderInfoLog(vertexID, infoLogLength, NULL, errorMsg);
        Logging::error("Could not compile vertex shader:\n%s\n", errorMsg);
        goto FAIL_VERTEX;
    }
    
    // compile fragment shader
    fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentID, 1, &fragmentShader, NULL);
    glCompileShader(fragmentID);
    glGetShaderiv(fragmentID, GL_COMPILE_STATUS, &result);
    glGetShaderiv(fragmentID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (result != GL_TRUE && infoLogLength > 0)
    {
        char errorMsg[infoLogLength];
        glGetShaderInfoLog(fragmentID, infoLogLength, NULL, errorMsg);
        Logging::error("Could not compile fragment shader:\n%s\n", errorMsg);
        goto FAIL_FRAGMENT;
    }
    
    // link program
    _program = glCreateProgram();
    glAttachShader(_program, vertexID);
    glAttachShader(_program, fragmentID);
    glLinkProgram(_program);
    glGetProgramiv(_program, GL_LINK_STATUS, &result);
    glGetProgramiv(_program, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (result != GL_TRUE && infoLogLength > 0)
    {
        char errorMsg[infoLogLength];
        glGetProgramInfoLog(_program, infoLogLength, NULL, errorMsg);
        Logging::error("Could not link shader program:\n%s\n", errorMsg);
        glDeleteProgram(_program);
        _program = 0;
    }
    
    // delete shaders as we don't need them anymore
FAIL_FRAGMENT:
    glDeleteShader(fragmentID);
FAIL_VERTEX:
    glDeleteShader(vertexID);    
}

Shader::~Shader(void)
{
    useDefault();
    if (_program != 0)
    {
        glDeleteProgram(_program);
    }
}

unsigned int Shader::getUniform(const char* name) const
{
    return glGetUniformLocation(_program, name);
}

void Shader::use(void)
{
    if (_program != 0)
    {
        glUseProgram(_program);
    }
}

void Shader::useDefault(void)
{
    glUseProgram(0);
}

char* Shader::readShaderFile(const char* shaderPath)
{
    SDL_RWops* shaderFile;
    long shaderLength;
    char* shader;
    
    shaderFile = SDL_RWFromFile(shaderPath, "r");
    if (shaderFile != NULL)
    {
        SDL_RWseek(shaderFile, 0, SEEK_END);
        shaderLength = SDL_RWtell(shaderFile);
    }
    // if the file couldn't be opened, we'll just return the empty string
    else
    {
        shaderLength = 0;
    }
    shader = new char[shaderLength + 1];
    if (shaderFile != NULL)
    {
        SDL_RWseek(shaderFile, 0, SEEK_SET);
        SDL_RWread(shaderFile, shader, 1, shaderLength);
        SDL_RWclose(shaderFile);
    }
    shader[shaderLength] = '\0';
    
    return shader;
}
