//
//  Screen.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Screen.h"

#include "GLEW/glew.h"

#include "Drawable.h"
#include "Logging.h"

static const float nearPlane = 0.1f;
static const float farPlane = 30.0f;

Screen::Screen(int width, int height)
: _width(width)
, _height(height)
{
    _inited = true;
    Logging::info("Initializing screen... ");
    
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        Logging::error("Could not initialize SDL: %s\n", SDL_GetError());
        _inited = false;
        return;
    }
    
    _window = SDL_CreateWindow("SimpleChess", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _width, _height, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
    if (_window == NULL)
    {
        Logging::error("Could not create game window: %s\n", SDL_GetError());
        shutdown();
        return;
    }
    
    _glContext = SDL_GL_CreateContext(_window);
    SDL_GL_MakeCurrent(_window, _glContext);
    
    // we need to make the SDL GL context the current one before initializing GLEW
    // otherwise Bad Things happen
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        Logging::error("Could not initialize GLEW\n");
        shutdown();
        return;
    }
    
    // VBOs and shaders require GL 2+
    if (!GLEW_VERSION_2_0)
    {
        Logging::error("Could not find OpenGL 2+\n");
        shutdown();
        return;
    }
    
    // we'll calc ortho and perspective matrices here and store them
    // since they don't change
    _orthoMat = mat4f::ortho(0, _width, _height, 0, -1.0f, 1.0f);
    _perspMat = mat4f::persp(M_PI / 2.0f, (float)_width / (float)_height, nearPlane, farPlane);
    
    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    glClearDepth(1.0);
    
    if (SDL_GL_SetSwapInterval(1) < 0)
    {
        // failing to set vsync isn't a fatal error, it just means the game will look uglier
        Logging::warn("Could not set vsync: %s\n", SDL_GetError());
    }
    
    Logging::info("done.\n");
}

Screen::~Screen(void)
{
    shutdown();
}

void Screen::shutdown(void)
{
    if (_inited)
    {
        Logging::info("Shutting down screen... ");
        
        SDL_GL_DeleteContext(_glContext);
        if (_window != NULL)
        {
            SDL_DestroyWindow(_window);
            _window = NULL;
        }
        SDL_Quit();
        
        Logging::info("done.\n");
        _inited = false;
    }
}

pt3f Screen::project(const pt3f& worldPoint, const mat4f& mvpMat) const
{
    pt4f inPt = {worldPoint.x, worldPoint.y, worldPoint.z, 1.0f};
    pt4f outPt = mvpMat * inPt;
    pt3f result = pt3f::make(outPt.x, outPt.y, outPt.z) / outPt.w;
    return result;
}

pt3f Screen::unProject(const pt3f& ndcPoint, const mat4f& mvpMat) const
{
    mat4f invMat = mvpMat.inv();
    pt4f inPt = {ndcPoint.x, ndcPoint.y, ndcPoint.z, 1.0f};
    pt4f outPt = invMat * inPt;
    // don't forget to do perspective division!
    pt3f result = pt3f::make(outPt.x, outPt.y, outPt.z) / outPt.w;
    return result;
}

void Screen::addDisplayElement(Drawable& element)
{
    _displayElements.push_back(&element);
}

void Screen::draw(void) const
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    for (DisplayElementContainer::const_iterator elementIter = _displayElements.begin(); elementIter != _displayElements.end(); elementIter++)
    {
        (*elementIter)->draw(*this);
    }
    
    SDL_GL_SwapWindow(_window);
}
