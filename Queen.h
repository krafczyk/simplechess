//
//  Queen.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Queen_h
#define SimpleChess_Queen_h

#include "Piece.h"

class Queen : public Piece
{
public:
    Queen(Color color, Model& model)
    : Piece(color, model) {};
    ~Queen(void) {};
    
    void getValidMoves(Board& board, std::set<Tile*>& moveList) const;
    
};

#endif
