//
//  Board.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Board_h
#define SimpleChess_Board_h

#include <map>
#include <vector>

#include "GLEW/glew.h"

#include "Drawable.h"
#include "mat4.h"
#include "vec3.h"
#include "ReceivesEvents.h"
#include "Updatable.h"

class Badge;
class EventManager;
class Piece;
class Shader;
class Tile;

using namespace math;

class Board : public Drawable, public Updatable, public ReceivesEvents
{
public:
    Board(int width, int height, EventManager& eventManager);
    ~Board(void);
    
    // in grid units
    int getWidth(void) const { return _width; };
    int getHeight(void) const { return _height; };
    
    // a tile is considered open if it has valid coordinates
    // and is unoccupied by a piece
    bool isTileOpen(int x, int y) const;
    // NULL if invalid grid coordinates
    Tile* getTile(int x, int y) const;
    // NULL if ray hit no tile
    // tileCoords only updated if hit is found
    Tile* findTile(Screen& screen, vec3f ray, pt2i* tileCoords) const;
    
    void placePiece(Piece& piece, int x, int y, bool animate = false);
    void removePiece(Piece& piece);
    void clear(void);
    
    bool isAnimatingPiece(void) const { return _animatingPiece; };

    void draw(const Screen& screen) const;
    
    void update(int deltaMS);
    
    bool receiveEvent(Event& event);
        
private:
    EventManager& _eventManager;
    
    Shader* _shader;
    GLuint _vbo;
    
    std::vector<Tile*> _tiles;
    int _width;
    int _height;
    
    typedef std::map<const Piece*, Tile*> OccupiedTileContainer;
    OccupiedTileContainer _occupiedTiles;
    
    bool _animatingPiece;
    Piece* _movingPiece;
    Tile* _destTile;
    long _pieceAnimateTime;
    
    bool _animatingVFX;
    Badge* _vfx;
    pt3f _vfxWorldBase;
    int _vfxYOffset;
    long _vfxAnimateTime;

};

#endif
