//
//  Logging.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Logging_h
#define SimpleChess_Logging_h

class Logging
{
public:
    static void info(const char* fmt, ...);
    static void warn(const char* fmt, ...);
    static void error(const char* fmt, ...);
    
    static void increaseIndentLevel(void);
    static void decreaseIndentLevel(void);
    
private:
    static int _indentLevel;
    static bool _shouldIndent;
    
};

#endif
