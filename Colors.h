//
//  Colors.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/15/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Colors_h
#define SimpleChess_Colors_h

#include "rgb.h"

namespace Colors
{
    static const geom::rgbf darkGray =
    {
        0.15f, 0.15f, 0.15f
    };
    
    static const geom::rgbf gray =
    {
        0.25f, 0.25f, 0.25f
    };
    
    static const geom::rgbf black =
    {
        0.0f, 0.0f, 0.0f
    };
    
    static const geom::rgbf white =
    {
        1.0f, 1.0f, 1.0f
    };

    static const geom::rgbf red =
    {
        1.0f, 0.0f, 0.0f
    };
    
    static const geom::rgbf green =
    {
        0.0f, 1.0f, 0.0f
    };
    
    static const geom::rgbf blue =
    {
        0.5f, 0.5f, 1.0f
    };
    
    static const geom::rgbf yellow =
    {
        1.0f, 1.0f, 0.0f
    };
}

#endif
