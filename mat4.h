//
//  mat4.h
//
//  Created by Keith Kaisershot on 10/4/13.
//  Copyright (c) 2013 Keith Kaisershot. All rights reserved.
//

#ifndef mat4_h
#define mat4_h

#include <math.h>

#include "pt4.h"
#include "vec3.h"
#include "vec4.h"
#include "mat3.h"

namespace math
{
    template<typename T>
    struct mat4
    {
        T m00;
        T m10;
        T m20;
        T m30;
        T m01;
        T m11;
        T m21;
        T m31;
        T m02;
        T m12;
        T m22;
        T m32;
        T m03;
        T m13;
        T m23;
        T m33;
        
        static mat4<T> make(
            T m00, T m01, T m02, T m03,
            T m10, T m11, T m12, T m13,
            T m20, T m21, T m22, T m23,
            T m30, T m31, T m32, T m33)
        {
            mat4<T> result = {
                m00, m10, m20, m30,
                m01, m11, m21, m31,
                m02, m12, m22, m32,
                m03, m13, m23, m33
            };
            return result;
        }
        
        const T& operator[](const size_t i) const
        {
            return this->*_elem[i];
        }
        
        T& operator[](const size_t i)
        {
            return this->*_elem[i];
        }
        
        const T& operator()(const size_t row, const size_t col) const
        {
            return this->*_elem[row + (col * 4)];
        }
        
        T& operator()(const size_t row, const size_t col)
        {
            return this->*_elem[row + (col * 4)];
        }
        
        mat4<T> operator+(const mat4<T>& rhs) const
        {
            mat4<T> result = {
                m00 + rhs.m00, m10 + rhs.m10, m20 + rhs.m20, m30 + rhs.m30,
                m01 + rhs.m01, m11 + rhs.m11, m21 + rhs.m21, m31 + rhs.m31,
                m02 + rhs.m02, m12 + rhs.m12, m22 + rhs.m22, m32 + rhs.m32,
                m03 + rhs.m03, m13 + rhs.m13, m23 + rhs.m23, m33 + rhs.m33
            };
            return result;
        }
        
        mat4<T> operator-(const mat4<T>& rhs) const
        {
            mat3<T> result = {
                m00 - rhs.m00, m10 - rhs.m10, m20 - rhs.m20, m30 - rhs.m30,
                m01 - rhs.m01, m11 - rhs.m11, m21 - rhs.m21, m31 - rhs.m31,
                m02 - rhs.m02, m12 - rhs.m12, m22 - rhs.m22, m32 - rhs.m32,
                m03 - rhs.m03, m13 - rhs.m13, m23 - rhs.m23, m33 - rhs.m33
            };
            return result;
        }
        
        mat4<T> operator*(const T rhs) const
        {
            mat4<T> result = {
                m00 * rhs, m10 * rhs, m20 * rhs, m30 * rhs,
                m01 * rhs, m11 * rhs, m21 * rhs, m31 * rhs,
                m02 * rhs, m12 * rhs, m22 * rhs, m32 * rhs,
                m03 * rhs, m13 * rhs, m23 * rhs, m33 * rhs
            };
            return result;
        }
        
        mat4<T> operator/(const T rhs) const
        {
            mat4<T> result = {
                m00 / rhs, m10 / rhs, m20 / rhs, m30 / rhs,
                m01 / rhs, m11 / rhs, m21 / rhs, m31 / rhs,
                m02 / rhs, m12 / rhs, m22 / rhs, m32 / rhs,
                m03 / rhs, m13 / rhs, m23 / rhs, m33 / rhs
            };
            return result;
        }
        
        mat4<T> operator*(const mat4<T>& rhs) const
        {
            mat4<T> result = {
                m00 * rhs.m00 + m01 * rhs.m10 + m02 * rhs.m20 + m03 * rhs.m30,
                m10 * rhs.m00 + m11 * rhs.m10 + m12 * rhs.m20 + m13 * rhs.m30,
                m20 * rhs.m00 + m21 * rhs.m10 + m22 * rhs.m20 + m23 * rhs.m30,
                m30 * rhs.m00 + m31 * rhs.m10 + m32 * rhs.m20 + m33 * rhs.m30,
                m00 * rhs.m01 + m01 * rhs.m11 + m02 * rhs.m21 + m03 * rhs.m31,
                m10 * rhs.m01 + m11 * rhs.m11 + m12 * rhs.m21 + m13 * rhs.m31,
                m20 * rhs.m01 + m21 * rhs.m11 + m22 * rhs.m21 + m23 * rhs.m31,
                m30 * rhs.m01 + m31 * rhs.m11 + m32 * rhs.m21 + m33 * rhs.m31,
                m00 * rhs.m02 + m01 * rhs.m12 + m02 * rhs.m22 + m03 * rhs.m32,
                m10 * rhs.m02 + m11 * rhs.m12 + m12 * rhs.m22 + m13 * rhs.m32,
                m20 * rhs.m02 + m21 * rhs.m12 + m22 * rhs.m22 + m23 * rhs.m32,
                m30 * rhs.m02 + m31 * rhs.m12 + m32 * rhs.m22 + m33 * rhs.m32,
                m00 * rhs.m03 + m01 * rhs.m13 + m02 * rhs.m23 + m03 * rhs.m33,
                m10 * rhs.m03 + m11 * rhs.m13 + m12 * rhs.m23 + m13 * rhs.m33,
                m20 * rhs.m03 + m21 * rhs.m13 + m22 * rhs.m23 + m23 * rhs.m33,
                m30 * rhs.m03 + m31 * rhs.m13 + m32 * rhs.m23 + m33 * rhs.m33,
            };
            return result;
        }
        
        pt4<T> operator*(const pt4<T>& rhs) const
        {
            pt4<T> result = {
                (m00 * rhs.x) + (m01 * rhs.y) + (m02 * rhs.z) + (m03 * rhs.w),
                (m10 * rhs.x) + (m11 * rhs.y) + (m12 * rhs.z) + (m13 * rhs.w),
                (m20 * rhs.x) + (m21 * rhs.y) + (m22 * rhs.z) + (m23 * rhs.w),
                (m30 * rhs.x) + (m31 * rhs.y) + (m32 * rhs.z) + (m33 * rhs.w)
            };
            return result;
        }
        
        vec4<T> operator*(const vec4<T>& rhs) const
        {
            vec4<T> result = {
                (m00 * rhs.x) + (m01 * rhs.y) + (m02 * rhs.z) + (m03 * rhs.w),
                (m10 * rhs.x) + (m11 * rhs.y) + (m12 * rhs.z) + (m13 * rhs.w),
                (m20 * rhs.x) + (m21 * rhs.y) + (m22 * rhs.z) + (m23 * rhs.w),
                (m30 * rhs.x) + (m31 * rhs.y) + (m32 * rhs.z) + (m33 * rhs.w)
            };
            return result;
        }
        
        mat3<T> minor(const size_t row, const size_t col) const
        {
            mat3<T> result;
            for (size_t i = 0, x = 0; i < 4; ++i)
            {
                if (i != col)
                {
                    for (size_t k = 0, y = 0; k < 4; ++k)
                    {
                        if (k != row)
                        {
                            result(y++, x) = (*this)(k, i);
                        }
                    }
                    ++x;
                }
            }
            return result;
        }
        
        float det() const
        {
            return
                m00 * minor(0, 0).det() -
                m01 * minor(0, 1).det() +
                m02 * minor(0, 2).det() -
                m03 * minor(0, 3).det();
        }
        
        float cofactor(const size_t row, const size_t col) const
        {
            return minor(row, col).det() * ((int)((row + col) % 2) * -2 + 1);
        }
        
        mat4<T> adj() const
        {
            mat4<T> result = {
                cofactor(0, 0), cofactor(0, 1), cofactor(0, 2), cofactor(0, 3),
                cofactor(1, 0), cofactor(1, 1), cofactor(1, 2), cofactor(1, 3),
                cofactor(2, 0), cofactor(2, 1), cofactor(2, 2), cofactor(2, 3),
                cofactor(3, 0), cofactor(3, 1), cofactor(3, 2), cofactor(3, 3)
            };
            return result;
        }
        
        mat4<T> inv() const
        {
            return adj() / det();
        }
        
        mat4<T> transpose() const
        {
            mat4<T> result = {
                m00, m01, m02, m03,
                m10, m11, m12, m13,
                m20, m21, m22, m23,
                m30, m31, m32, m33
            };
            return result;
        }
        
        static mat4<T> ident()
        {
            mat4<T> result = {
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        static mat4<T> rotX(const float radians)
        {
            float c = cosf(radians);
            float s = sinf(radians);
            mat4<T> result = {
                1, 0, 0, 0,
                0, c, -s, 0,
                0, s, c, 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        static mat4<T> rotY(const float radians)
        {
            float c = cosf(radians);
            float s = sinf(radians);
            mat4<T> result = {
                c, 0, s, 0,
                0, 1, 0, 0,
                -s, 0, c, 0,
                0, 0, 0, 1
            };
            return result;
        }
                                                                                               
        static mat4<T> rotZ(const float radians)
        {
            float c = cosf(radians);
            float s = sinf(radians);
            mat4<T> result = {
                c, -s, 0, 0,
                s, c, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        static mat4<T> scale(const float scale)
        {
            mat4<T> result = {
                scale, 0, 0, 0,
                0, scale, 0, 0,
                0, 0, scale, 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        static mat4<T> trans(const vec3<T>& v)
        {
            mat4<T> result = {
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                v.x, v.y, v.z, 1
            };
            return result;
        }
        
        static mat4<T> shearXY(const float s)
        {
            mat4<T> result = {
                1, 0, 0, 0,
                s, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        static mat4<T> shearXZ(const float s)
        {
            mat4<T> result = {
                1, 0, 0, 0,
                0, 1, 0, 0,
                s, 0, 1, 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        static mat4<T> shearYX(const float s)
        {
            mat4<T> result = {
                1, s, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        static mat4<T> shearYZ(const float s)
        {
            mat4<T> result = {
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, s, 1, 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        static mat4<T> shearZX(const float s)
        {
            mat4<T> result = {
                1, 0, s, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        static mat4<T> shearZY(const float s)
        {
            mat4<T> result = {
                1, 0, 0, 0,
                0, 1, s, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        static mat4<T> rotAxis(const vec3<T>& normalized, const float radians)
        {
            float c = cosf(radians);
            float s = sinf(radians);
            mat4<T> result = {
                c + ((1 - c) * normalized.x * normalized.x), ((1 - c) * normalized.x * normalized.y) + (normalized.z * s), ((1 - c) * normalized.x * normalized.z) - (normalized.y * s), 0,
                ((1 - c) * normalized.x * normalized.y) - (normalized.z * s), c + ((1 - c) * normalized.y * normalized.y), ((1 - c) * normalized.y * normalized.z) + (normalized.x * s), 0,
                ((1 - c) * normalized.x * normalized.z) + (normalized.y * s), ((1 - c) * normalized.y * normalized.z) - (normalized.x * s), c + ((1 - c) * normalized.z * normalized.z), 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        static mat4<T> rotToVec(const vec3<T>& src, const vec3<T>& dst)
        {
            vec3<T> unitSrc = src.normalized();
            vec3<T> unitDst = dst.normalized();
            vec3<T> cross = unitSrc.cross(unitDst);
            float e = unitSrc.dot(unitDst);
            float h = 1 / (1 + e);
            mat4<T> result = {
                e + (h * unitSrc.x * unitSrc.x), (h * unitSrc.x * unitSrc.y) + unitSrc.z, (h * unitSrc.x * unitSrc.z) - unitSrc.y, 0,
                (h * unitSrc.x * unitSrc.y) - unitSrc.z, e + (h * unitSrc.y * unitSrc.y), (h * unitSrc.y * unitSrc.z) + unitSrc.x, 0,
                (h * unitSrc.x * unitSrc.z) + unitSrc.y, (h * unitSrc.y * unitSrc.z) - unitSrc.x, e + (h * unitSrc.z * unitSrc.z), 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        static mat4<T> rotToUnitVec(const vec3<T>& src, const vec3<T>& dst)
        {
            vec3<T> cross = src.cross(dst);
            float e = src.dot(dst);
            float h = 1 / (1 + e);
            mat4<T> result = {
                e + (h * src.x * src.x), (h * src.x * src.y) + src.z, (h * src.x * src.z) - src.y, 0,
                (h * src.x * src.y) - src.z, e + (h * src.y * src.y), (h * src.y * src.z) + src.x, 0,
                (h * src.x * src.z) + src.y, (h * src.y * src.z) - src.x, e + (h * src.z * src.z), 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        static mat4<T> ortho(const float l, const float r, const float b, const float t, const float n, const float f)
        {
            mat4<T> result = {
                2 / (r - l), 0, 0, 0,
                0, 2 / (t - b), 0, 0,
                0, 0, -2 / (f - n), 0,
                -(r + l) / (r - l), -(t + b) / (t - b), -(f + n) / (f - n), 1
            };
            return result;
        }
        
        static mat4<T> frust(const float l, const float r, const float b, const float t, const float n, const float f)
        {
            mat4<T> result = {
                (2.0f * n) / (r - l), 0, 0, 0,
                0, (2.0f * n) / (t - b), 0, 0,
                (r + l) / (r - l), (t + b) / (t - b), -(f + n) / (f - n), -1,
                0, 0, -(2.0f * f * n) / (f - n), 0
            };
            return result;
        }
        
        static mat4<T> persp(const float fovRadians, const float aspect, const float n, const float f)
        {
            float s = 1 / tanf(fovRadians / 2);
            mat4<T> result = {
                s / aspect, 0, 0, 0,
                0, s, 0, 0,
                0, 0, -(f + n) / (f - n), -1,
                0, 0, -(2 * f * n) / (f - n), 0
            };
            return result;
        }
    private:
        static T mat4<T>::*_elem[4 * 4];
    };
    
    template<typename T>
    T mat4<T>::*mat4<T>::_elem[4 * 4] = {
        &mat4<T>::m00, &mat4<T>::m10, &mat4<T>::m20, &mat4<T>::m30,
        &mat4<T>::m01, &mat4<T>::m11, &mat4<T>::m21, &mat4<T>::m31,
        &mat4<T>::m02, &mat4<T>::m12, &mat4<T>::m22, &mat4<T>::m32,
        &mat4<T>::m03, &mat4<T>::m13, &mat4<T>::m23, &mat4<T>::m33
    };
    
    typedef mat4<char> mat4c;
    typedef mat4<short> mat4s;
    typedef mat4<int> mat4i;
    typedef mat4<float> mat4f;
}

#endif
