//
//  pt4.h
//
//  Created by Keith Kaisershot on 10/3/13.
//  Copyright (c) 2013 Keith Kaisershot. All rights reserved.
//

#ifndef pt4_h
#define pt4_h

namespace math
{
    template<typename T>
    struct pt4
    {
        T x;
        T y;
        T z;
        T w;
        
        static pt4<T> make(T x, T y, T z, T w)
        {
            pt4<T> result = {x, y, z, w};
            return result;
        }
        
        const T& operator[](size_t i) const
        {
            return this->*_elem[i];
        }
        
        T& operator[](size_t i)
        {
            return this->*_elem[i];
        }
        
        pt4<T> operator*(const T rhs) const
        {
            pt4<T> result = {x * rhs, y * rhs, z * rhs, w * rhs};
            return result;
        }
        
        pt4<T> operator/(const T rhs) const
        {
            pt4<T> result = {x / rhs, y / rhs, z / rhs, w / rhs};
            return result;
        }
    private:
        static T pt4<T>::*_elem[4];
    };
    
    template<typename T>
    T pt4<T>::*pt4<T>::_elem[4] = {&pt4<T>::x, &pt4<T>::y, &pt4<T>::z, &pt4<T>::w};
    
    typedef pt4<char> pt4c;
    typedef pt4<short> pt4s;
    typedef pt4<int> pt4i;
    typedef pt4<float> pt4f;
}

#endif
