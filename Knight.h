//
//  Knight.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Knight_h
#define SimpleChess_Knight_h

#include "Piece.h"

class Knight : public Piece
{
public:
    Knight(Color color, Model& model)
    : Piece(color, model) {};
    ~Knight(void) {};
    
    void getValidMoves(Board& board, std::set<Tile*>& moveList) const;
    
};

#endif
