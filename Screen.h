//
//  Screen.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Screen_h
#define SimpleChess_Screen_h

#include <GLEW/glew.h>
#include <vector>

#include "mat4.h"

class Drawable;

using namespace math;

class Screen
{
public:
    Screen(int width, int height);
    ~Screen(void);
    
    bool isInited(void) const { return _inited; };
    
    // in pixels
    int getWidth(void) const { return _width; };
    int getHeight(void) const { return _height; };
    
    const mat4f& getOrtho(void) const { return _orthoMat; };
    const mat4f& getPersp(void) const { return _perspMat; };
    
    pt3f project(const pt3f& worldPoint, const mat4f& mvpMat) const;
    pt3f unProject(const pt3f& ndcPoint, const mat4f& mvpMat) const;
    
    void addDisplayElement(Drawable& element);
    
    void draw(void) const;
        
private:
    bool _inited;
    
    typedef std::vector<Drawable*> DisplayElementContainer;
    DisplayElementContainer _displayElements;
    
    SDL_Window* _window;
    SDL_GLContext _glContext;
    int _width;
    int _height;
    
    mat4f _orthoMat;
    mat4f _perspMat;
        
    void shutdown(void);
    
};

#endif
