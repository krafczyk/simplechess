//
//  quat.h
//
//  Created by Keith Kaisershot on 10/4/13.
//  Copyright (c) 2013 Keith Kaisershot. All rights reserved.
//

#ifndef quat_h
#define quat_h

#include <math.h>

#include "mat4.h"

namespace math
{
    template<typename T>
    struct quat
    {
        T i;
        T j;
        T k;
        T w;
        
        static quat<T> make(T i, T j, T k, T w)
        {
            quat<T> result = {i, j, k, w};
            return result;
        }
        
        const T& operator[](size_t i) const
        {
            return this->*_elem[i];
        }
        
        T& operator[](size_t i)
        {
            return this->*_elem[i];
        }
        
        quat<T> operator-() const
        {
            quat<T> result = {-i, -j, -k, -w};
            return result;
        }
        
        void neg()
        {
            i = -i;
            j = -j;
            k = -k;
            w = -w;
        }
        
        quat<T> operator+(const quat<T>& rhs) const
        {
            quat<T> result = {i + rhs.i, j + rhs.j, k + rhs.k, w + rhs.w};
            return result;
        }
        
        quat<T> operator-(const quat<T>& rhs) const
        {
            quat<T> result = {i - rhs.i, j - rhs.j, k - rhs.k, w - rhs.w};
            return result;
        }
        
        quat<T> operator*(const T rhs) const
        {
            quat<T> result = {i * rhs, j * rhs, k * rhs, w * rhs};
            return result;
        }
        
        quat<T> operator/(const T rhs) const
        {
            quat<T> result = {i / rhs, j / rhs, k / rhs, w / rhs};
            return result;
        }
        
        operator mat4<T>() const
        {
            float s = 2 / this->norm();
            mat4<T> result = {
                1 - (s * ((j * j) + (k * k))), s * ((i * j) + (w * k)), s * ((i * k) - (w * j)), 0,
                s * ((i * j) - (w * k)), 1 - (s * ((i * i) + (k * k))), s * ((j * k) + (w * i)), 0,
                s * ((i * k) + (w * j)), s * ((j * k) - (w * i)), 1 - (s * ((i * i) + (j * j))), 0,
                0, 0, 0, 1
            };
            return result;
        }
        
        float dot(const quat<T>& rhs) const
        {
            return (i * rhs.i) + (j * rhs.j) + (k * rhs.k) + (w * rhs.w);
        }
        
        quat<T> conj() const
        {
            quat<T> result = {-i, -j, -k, w};
            return result;
        }
        
        float normSq() const
        {
            return (i * i) + (j * j) + (k * k) + (w * w);
        }
        
        float norm() const
        {
            return sqrtf(this->normSq());
        }
        
        quat<T> normalized() const
        {
            return *this / this->norm();
        }
        
        void normalize()
        {
            *this /= norm();
        }
        
        quat<T> inv() const
        {
            return this->conj() / this->normSq();
        }
        
        quat<T> operator*(const quat<T>& rhs) const
        {
            quat<T> result = {
                (j * rhs.k) - (k * rhs.j) + (rhs.w * i) + (w * rhs.i),
                (k * rhs.i) - (i * rhs.k) + (rhs.w * j) + (w * rhs.j),
                (i * rhs.j) - (j * rhs.i) + (rhs.w * k) + (w * rhs.k),
                (w * rhs.w) - (i * rhs.i) - (j * rhs.j) - (k * rhs.k)
            };
            return result;
        }
        
        quat<T> operator/(const quat<T>& rhs) const
        {
            return *this * rhs.inv();
        }
        
        quat<T> log() const
        {
            float a = acosf(w);
            float s = sinf(a);
            quat<T> result = {(a * i) / s, (a * j) / s, (a * k) / s, 0};
            return result;
        }
        
        quat<T> pow(const float t) const
        {
            float a = acosf(w);
            float s = sinf(a);
            float st = sinf(a * t);
            float ct = cosf(a * t);
            quat<T> result = {(st * i) / s, (st * j) / s, (st * k) / s, ct * w};
            return result;
        }
        
        quat<T> logUnit() const
        {
            float a = acosf(w);
            quat<T> result = {a * i, a * j, a * k, 0};
            return result;
        }
        
        quat<T> powUnit(const float t) const
        {
            float a = acosf(w);
            float s = sinf(a * t);
            float c = cosf(a * t);
            quat<T> result = {s * i, s * j, s * k, c * w};
            return result;
        }
        
        pt3<T> rot(const pt3<T>& rhs) const
        {
            quat<T> ptAsQuat = {rhs.x, rhs.y, rhs.z, 1};
            quat<T> result = *this * ptAsQuat * this->inv();
            pt3<T> quatAsPt = {result.i, result.j, result.k};
            return result;
        }
        
        pt3<T> rotUnit(const pt3<T>& rhs) const
        {
            quat<T> ptAsQuat = {rhs.x, rhs.y, rhs.z, 1};
            quat<T> result = *this * ptAsQuat * this->conj();
            pt3<T> quatAsPt = {result.i, result.j, result.k};
            return result;
        }
        
        pt4<T> rot(const pt4<T>& rhs) const
        {
            quat<T> ptAsQuat = {rhs.x, rhs.y, rhs.z, rhs.w};
            quat<T> result = *this * ptAsQuat * this->inv();
            pt4<T> quatAsPt = {result.i, result.j, result.k, result.w};
            return result;
        }
        
        pt4<T> rotUnit(const pt4<T>& rhs) const
        {
            quat<T> ptAsQuat = {rhs.x, rhs.y, rhs.z, rhs.w};
            quat<T> result = *this * ptAsQuat * this->conj();
            pt4<T> quatAsPt = {result.i, result.j, result.k, result.w};
            return result;
        }
        
        vec3<T> rot(const vec3<T>& rhs) const
        {
            quat<T> vecAsQuat = {rhs.x, rhs.y, rhs.z, 0};
            quat<T> result = *this * vecAsQuat * this->inv();
            vec3<T> quatAsVec = {result.i, result.j, result.k};
            return result;
        }
        
        vec3<T> rotUnit(const vec3<T>& rhs) const
        {
            quat<T> vecAsQuat = {rhs.x, rhs.y, rhs.z, 0};
            quat<T> result = *this * vecAsQuat * this->conj();
            vec3<T> quatAsVec = {result.i, result.j, result.k};
            return result;
        }
        
        vec4<T> rot(const vec4<T>& rhs) const
        {
            quat<T> vecAsQuat = {rhs.x, rhs.y, rhs.z, rhs.w};
            quat<T> result = *this * vecAsQuat * this->inv();
            vec4<T> quatAsVec = {result.i, result.j, result.k, result.w};
            return result;
        }
        
        vec4<T> rotUnit(const vec4<T>& rhs) const
        {
            quat<T> vecAsQuat = {rhs.x, rhs.y, rhs.z, rhs.w};
            quat<T> result = *this * vecAsQuat * this->conj();
            vec4<T> quatAsVec = {result.i, result.j, result.k, result.w};
            return result;
        }
        
        static quat<T> rotToVec(const vec3<T>& src, const vec3<T>& dst)
        {
            vec4<T> unitSrc = src.normalized();
            vec4<T> unitDst = dst.normalized();
            vec4<T> cross = unitSrc.cross(unitDst);
            float e = unitSrc.dot(unitDst);
            float coeff = 1 / sqrtf(2 * (1 - e));
            quat<T> result = {coeff * cross.x, coeff * cross.y, coeff * cross.z, sqrtf(2 * (1 + e)) / 2};
            return result;
        }
        
        static quat<T> rotToUnitVec(const vec3<T>& src, const vec3<T>& dst)
        {
            vec4<T> cross = src.cross(dst);
            float e = src.dot(dst);
            float coeff = 1 / sqrtf(2 * (1 - e));
            quat<T> result = {coeff * cross.x, coeff * cross.y, coeff * cross.z, sqrtf(2 * (1 + e)) / 2};
            return result;
        }
        
        static quat<T> slerp(const quat<T>& src, const quat<T>& dst, const float t)
        {
            float a = acosf(src.dot(dst));
            float s = sinf(a);
            quat<T> result = ((sinf(a * (1 - t)) / s) * src) + ((sinf(a * t) / s) * dst);
            return result;
        }
        
        static quat<T> squad(const quat<T>& src0, const quat<T>& src1, const quat<T>& dst0, const quat<T>& dst1, const float t)
        {
            return slerp(slerp(src0, src1, t), slerp(dst0, dst1, t), 2 * t * (1 - t));
        }
        
        static quat<T> ident()
        {
            quat<T> result = {0, 0, 0, 1};
            return result;
        }
        
        static quat<T> axisAngle(const vec3<T> normalized, const float radians)
        {
            float halfAngle = radians / 2;
            float s = sinf(halfAngle);
            quat<T> result = {normalized.x * s, normalized.y * s, normalized.z * s, cosf(halfAngle)};
            return result;
        }
        
        static quat<T> rotX(const float radians)
        {
            float halfAngle = radians / 2;
            quat<T> result = {sinf(halfAngle), 0, 0, cosf(halfAngle)};
            return result;
        }
        
        static quat<T> rotY(const float radians)
        {
            float halfAngle = radians / 2;
            quat<T> result = {0, sinf(halfAngle), 0, cosf(halfAngle)};
            return result;
        }
        
        static quat<T> rotZ(const float radians)
        {
            float halfAngle = radians / 2;
            quat<T> result = {0, 0, sinf(halfAngle), cosf(halfAngle)};
            return result;
        }
        
        static quat<T> euler(const float radiansX, const float radiansY, const float radiansZ)
        {
            float halfAngleX = radiansX / 2;
            float halfAngleY = radiansY / 2;
            float halfAngleZ = radiansZ / 2;
            float cx = cosf(halfAngleX);
            float cy = cosf(halfAngleY);
            float cz = cosf(halfAngleZ);
            float sx = sinf(halfAngleX);
            float sy = sinf(halfAngleY);
            float sz = sinf(halfAngleZ);
            quat<T> result = {
                (sx * cy * cz) - (cx * sy * sz),
                (cx * sy * cz) + (sx * cy * sz),
                (cx * cy * sz) - (sx * sy * cz),
                (cx * cy * cz) + (sx * sy * sz)
            };
            return result;
        }
    private:
        static T quat<T>::*_elem[4];
    };
    
    template<typename T>
    T quat<T>::*quat<T>::_elem[4] = {&quat<T>::i, &quat<T>::j, &quat<T>::k, &quat<T>::w};
    
    typedef quat<char> quatc;
    typedef quat<short> quats;
    typedef quat<int> quati;
    typedef quat<float> quatf;
}

#endif
