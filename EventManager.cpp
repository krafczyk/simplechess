//
//  EventManager.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/12/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "EventManager.h"

#include "Event.h"
#include "ReceivesEvents.h"

void EventManager::registerReceiver(ReceivesEvents& receiver)
{
    _receivers.insert(&receiver);
}

void EventManager::unregisterReceiver(ReceivesEvents& receiver)
{
    ReceiverContainer::iterator receiverIter = _receivers.find(&receiver);
    if (receiverIter != _receivers.end())
    {
        _receivers.erase(receiverIter);
    }
}

void EventManager::postEvent(Event& event)
{
    _events.push_back(&event);
}

void EventManager::flushEvents(void)
{
    for (EventContainer::iterator eventIter = _events.begin(); eventIter != _events.end(); eventIter++)
    {
        for (ReceiverContainer::iterator receiverIter = _receivers.begin(); receiverIter != _receivers.end(); receiverIter++)
        {
            if (!(*receiverIter)->receiveEvent(**eventIter))
            {
                break;
            }
        }
        delete *eventIter;
    }
    _events.clear();
}
