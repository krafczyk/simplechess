//
//  SoundManager.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_SoundManager_h
#define SimpleChess_SoundManager_h

#include <map>

#include "ReceivesEvents.h"

class Sound;

class SoundManager : public ReceivesEvents
{
public:
    SoundManager(void);
    ~SoundManager(void);
    
    bool isInited(void) const { return _inited; };

    // addSound maps a sound to an event type
    // when that event is fired, the sound is automatically played
    void addSound(Sound& sound, int eventType);
    
    bool receiveEvent(Event& event);
    
private:
    bool _inited;
    
    std::map<int, Sound*> _sounds;
    
    void shutdown(void);
    
};

#endif
