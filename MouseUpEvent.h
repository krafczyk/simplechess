//
//  MouseUpEvent.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_MouseUpEvent_h
#define SimpleChess_MouseUpEvent_h

#include "MouseEvent.h"

class MouseUpEvent : public MouseEvent
{
public:
    MouseUpEvent(float x, float y) 
    : MouseEvent('up  ', x, y) {};
    ~MouseUpEvent(void) {};
    
};

#endif
