//
//  Game.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/12/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Game.h"

#include "tiny_obj_loader.h"

#include "Bishop.h"
#include "Board.h"
#include "CaptureEvent.h"
#include "Colors.h"
#include "Event.h"
#include "EventManager.h"
#include "HUD.h"
#include "Knight.h"
#include "Logging.h"
#include "Model.h"
#include "MouseDownEvent.h"
#include "MouseMoveEvent.h"
#include "MouseUpEvent.h"
#include "Piece.h"
#include "Queen.h"
#include "QuitEvent.h"
#include "ScoreEvent.h"
#include "Screen.h"
#include "Sound.h"
#include "SoundManager.h"
#include "TakeTurnEvent.h"
#include "Tile.h"
#include "Timer.h"

using namespace math;

Game::Game(void)
{
    _inited = true;
    Logging::info("Initializing game... ");
    Logging::increaseIndentLevel();
        
    _screen = new Screen(1024, 768);
    if (!_screen->isInited())
    {
        _inited = false;
        return;
    }
    
    _eventManager = new EventManager();
    _timer = new Timer(SDL_GetTicks());
    
    _soundManager = new SoundManager();
    _captureSound = new Sound("score.wav");
    _soundManager->addSound(*_captureSound, 'capt');

    _hud = new HUD();
    _board = new Board(8, 8, *_eventManager);
    
    _timer->addListener(*_board);
    for (int y = 0; y < _board->getHeight(); ++y)
    {
        for (int x = 0; x < _board->getWidth(); ++x)
        {
            _timer->addListener(*_board->getTile(x, y));
        }
    }
    
    // add elements to the screen in draw order
    _screen->addDisplayElement(*_board);
    _screen->addDisplayElement(*_hud);
    
    _eventManager->registerReceiver(*this);
    _eventManager->registerReceiver(*_hud);
    _eventManager->registerReceiver(*_board);
    _eventManager->registerReceiver(*_soundManager);
    
    Logging::info("Loading models... ");
    _pieceModels[0] = new Model("Bishop.obj", "");
    _pieceModels[1] = new Model("Queen.obj", "");
    _pieceModels[2] = new Model("Knight.obj", "");
    Logging::info("done.\n");
    
    Logging::info("Creating pieces... ");
    _pieces.push_back(new Bishop(Piece::BLACK, *_pieceModels[0]));
    _pieces.push_back(new Bishop(Piece::WHITE, *_pieceModels[0]));
    _pieces.push_back(new Queen(Piece::BLACK, *_pieceModels[1]));
    _pieces.push_back(new Queen(Piece::WHITE, *_pieceModels[1]));
    _pieces.push_back(new Knight(Piece::BLACK, *_pieceModels[2]));
    _pieces.push_back(new Knight(Piece::WHITE, *_pieceModels[2]));
    Logging::info("done.\n");
    
    srand((unsigned int)time(NULL));
    reset();
    
    _running = true;
    Logging::decreaseIndentLevel();
    Logging::info("done.\n");
}

Game::~Game(void)
{
    shutdown();
}

void Game::shutdown(void)
{
    if (_inited)
    {
        Logging::info("Shutting down game... ");
        Logging::increaseIndentLevel();
        
        delete _hud;
        delete _board;
        
        Logging::info("Throwing away pieces... ");
        for (PieceContainer::iterator pieceIter = _pieces.begin(); pieceIter != _pieces.end(); pieceIter++)
        {
            delete *pieceIter;
        }
        Logging::info("done.\n");

        Logging::info("Destroying models... ");
        int modelCount = sizeof(_pieceModels) / sizeof(*_pieceModels);
        for (int i = 0; i < modelCount; ++i)
        {
            delete _pieceModels[i];
        }
        Logging::info("done.\n");
        
        if (_timer != NULL)
        {
            delete _timer;
            _timer = NULL;
        }
        if (_eventManager != NULL)
        {
            delete _eventManager;
            _eventManager = NULL;
        }
        
        if (_screen != NULL)
        {
            delete _screen;
            _screen = NULL;
        }
        
        Logging::decreaseIndentLevel();
        Logging::info("done.\n");
        _inited = false;
    }
}

void Game::reset(void)
{
    _board->clear();
    
    int tileIndices = _board->getWidth() * _board->getHeight();
    std::vector<int> indexBucket;
    indexBucket.reserve(tileIndices);
    for (int i = 0; i < tileIndices; ++i)
    {
        indexBucket.push_back(i);
    }
    
    // randomize piece placement
    int modelCount = sizeof(_pieceModels) / sizeof(*_pieceModels);
    for (int i = 0; i < modelCount * 2; ++i)
    {
        int bucketIndex = rand() % (int)indexBucket.size();
        int destIndex = indexBucket[bucketIndex];
        indexBucket.erase(indexBucket.begin() + bucketIndex);
        
        int destX = destIndex % _board->getWidth();
        int destY = destIndex / _board->getWidth();
        _board->placePiece(*_pieces[i], destX, destY);
    }
    
    // reset the scores
    _scores[Piece::BLACK] = 0;
    _scores[Piece::WHITE] = 0;
    
    _eventManager->postEvent(*new ScoreEvent(_scores[Piece::BLACK], _scores[Piece::WHITE]));
    
    _interestingTile = NULL;
    _selectedTile = NULL;
    
    // white always goes first in traditional chess
    _currentPlayer = Piece::WHITE;
    _eventManager->postEvent(*new TakeTurnEvent(_currentPlayer));
}

bool Game::handleInput(void)
{
    SDL_Event event;
    
    // only handle input if there are events waiting
    if (SDL_PollEvent(&event))
    {
        do
        {
            switch (event.type)
            {
                case SDL_QUIT:
                    _eventManager->postEvent(*new QuitEvent());
                    break;
                // we'll convert screen coords into ndc coords to make unprojection easier
                case SDL_MOUSEMOTION:
                    {
                        int x, y;
                        SDL_GetMouseState(&x, &y);
                        _eventManager->postEvent(
                            *new MouseMoveEvent((float)x / (float)_screen->getWidth() * 2.0f - 1.0f,
                                                (float)y / (float)_screen->getHeight() * -2.0f + 1.0f));
                    }
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    {
                        int x, y;
                        SDL_GetMouseState(&x, &y);
                        _eventManager->postEvent(
                             *new MouseDownEvent((float)x / (float)_screen->getWidth() * 2.0f - 1.0f,
                                                 (float)y / (float)_screen->getHeight() * -2.0f + 1.0f));
                    }
                    break;
                case SDL_MOUSEBUTTONUP:
                    {
                        int x, y;
                        SDL_GetMouseState(&x, &y);
                        _eventManager->postEvent(
                             *new MouseUpEvent((float)x / (float)_screen->getWidth() * 2.0f - 1.0f,
                                               (float)y / (float)_screen->getHeight() * -2.0f + 1.0f));
                    }
                    break;
                default:
                    break; 
            }
        }
        while (SDL_PollEvent(&event));
        return true;
    }
    
    return false;    
}

void Game::processFrame(void)
{
    handleInput();
    _eventManager->flushEvents();
    _timer->serviceTimers(SDL_GetTicks());
    _screen->draw();
}

bool Game::receiveEvent(Event& event)
{
    switch (event.type())
    {
        case 'quit':
            _running = false;
            break;
        case 'move':
        case 'down':
        case 'up  ':
            {
                if (_board->isAnimatingPiece())
                {
                    break;
                }
            
                MouseEvent* mouseEvent = (MouseEvent*)&event;
                
                // get a tile in 3D space from mouse coords in 2D space
                pt3f ndcPt = {mouseEvent->x(), mouseEvent->y(), 0.0f};
                mat4f mvpMat = _screen->getPersp();
                pt3f worldPt = _screen->unProject(ndcPt, mvpMat);
                vec3f ray = vec3f::make(worldPt.x, worldPt.y, worldPt.z).normalized();
                
                pt2i tileCoords;
                bool foundTile = _board->findTile(*_screen, ray, &tileCoords);
                switch (mouseEvent->type())
                {
                    case 'move':
                        if (foundTile)
                        {
                            Tile* thisTile = _board->getTile(tileCoords.x, tileCoords.y);
                            const Piece* thisPiece = thisTile->getPiece();
                            // are we hovering over something different than before?
                            if (_interestingTile != thisTile)
                            {
                                // de-hilite any hint tiles that we were previously interested in
                                if (_interestingTile != NULL)
                                {
                                    // if one of the hint tiles was our selected tile
                                    // then restore it to selected again
                                    if (_interestingTile == _selectedTile)
                                    {
                                        _selectedTile->setHiliteColor(Colors::green);
                                    }
                                    else
                                    {
                                        _interestingTile->setHilite(false);
                                    }
                                }
                                for (ValidMoveContainer::iterator hintIter = _hintTiles.begin(); hintIter != _hintTiles.end(); hintIter++)
                                {
                                    if (*hintIter == _selectedTile)
                                    {
                                        _selectedTile->setHiliteColor(Colors::green);
                                    }
                                    else
                                    {
                                        (*hintIter)->setHilite(false);
                                    }
                                }
                                // are we hovering over a piece?
                                if (thisPiece != NULL)
                                {
                                    _interestingTile = thisTile;
                                    // if this piece is the opposite color
                                    // then hilite its tile in red to hint the player
                                    if (thisPiece->getColor() != _currentPlayer)
                                    {
                                        _interestingTile->setHilite(true);
                                        _interestingTile->setHiliteColor(Colors::red);
                                    }
                                    thisPiece->getValidMoves(*_board, _hintTiles);
                                    // hilite all valid moves, with possible captures in a special color
                                    for (ValidMoveContainer::iterator hintIter = _hintTiles.begin(); hintIter != _hintTiles.end(); hintIter++)
                                    {
                                        (*hintIter)->setHilite(true);
                                        const Piece* hintPiece = (*hintIter)->getPiece();
                                        if (hintPiece && hintPiece->getColor() != thisPiece->getColor())
                                        {
                                            (*hintIter)->setHiliteColor(Colors::yellow);
                                        }
                                        else
                                        {
                                            (*hintIter)->setHiliteColor(Colors::blue);
                                        }
                                    }
                                }
                                // otherwise don't give any hints
                                else
                                {
                                    if (_interestingTile != NULL)
                                    {
                                        if (_interestingTile == _selectedTile)
                                        {
                                            _selectedTile->setHiliteColor(Colors::green);
                                        }
                                        else
                                        {
                                            _interestingTile->setHilite(false);
                                        }
                                    }
                                    _interestingTile = NULL;
                                }
                            }
                        }
                        // if we clicked outside the board then de-hilite all hint tiles
                        else if (_interestingTile != NULL)
                        {
                            _interestingTile->setHilite(false);
                            for (ValidMoveContainer::iterator hintIter = _hintTiles.begin(); hintIter != _hintTiles.end(); hintIter++)
                            {
                                (*hintIter)->setHilite(false);
                            }
                            _interestingTile = NULL;
                        }
                        break;
                    case 'down':
                        if (foundTile)
                        {
                            Tile* tile = _board->getTile(tileCoords.x, tileCoords.y);
                            Piece* piece = tile->getPiece();
                            // if we clicked one of our own pieces, then deselect the last piece
                            // and select this one
                            if (piece != NULL && piece->getColor() == _currentPlayer &&
                                (_selectedTile == NULL || _selectedTile->getPiece() != piece))
                            {
                                if (_selectedTile != NULL)
                                {
                                    _selectedTile->setHilite(false);
                                }
                                _selectedTile = tile;
                                _selectedTile->setHiliteColor(Colors::green);
                                _selectedTile->setHilite(true);
                            }
                            // else we clicked on a tile we don't control 
                            else if (_selectedTile != NULL)
                            {
                                ValidMoveContainer validMoves;
                                _selectedTile->getPiece()->getValidMoves(*_board, validMoves);
                                // is this a valid move?
                                if (validMoves.find(tile) != validMoves.end())
                                {
                                    // de-hilite all hint tiles
                                    if (_interestingTile != NULL)
                                    {
                                        _interestingTile->setHilite(false);
                                    }
                                    for (ValidMoveContainer::iterator hintIter = _hintTiles.begin(); hintIter != _hintTiles.end(); hintIter++)
                                    {
                                        (*hintIter)->setHilite(false);
                                    }
                                    
                                    // animate our piece moving to its new location, taking the other piece if present
                                    _board->placePiece(*_selectedTile->getPiece(), tileCoords.x, tileCoords.y, true);
                                    
                                    // increase the score if the other piece belonged to the other player
                                    if (piece != NULL && piece->getColor() != _currentPlayer)
                                    {
                                        _scores[_currentPlayer]++;
                                        _eventManager->postEvent(*new ScoreEvent(_scores[Piece::BLACK], _scores[Piece::WHITE]));
                                    }
                                    
                                    // next player's turn if there are pieces left
                                    if (_scores[_currentPlayer] != _pieces.size() / 2)
                                    {
                                        _currentPlayer = (Piece::Color)((_currentPlayer + 1) % Piece::NUM_COLORS);
                                        _eventManager->postEvent(*new TakeTurnEvent(_currentPlayer));
                                    }
                                }
                                
                                // deselect our tile
                                _selectedTile->setHilite(false);
                                _selectedTile = NULL;
                            }
                        }
                        // if we clicked outside the board then deselect our tile
                        else if (_selectedTile != NULL)
                        {
                            _selectedTile->setHilite(false);
                            _selectedTile = NULL;
                        }
                        break;
                    case 'up  ':
                        // if we clicked on the "reset" button, then start a new game
                        if (_hud->click(*_screen, pt2f::make(mouseEvent->x(), mouseEvent->y())))
                        {
                            reset();
                            break;
                        }
                        break;
                    default:
                        break;
                }
                
            }
            break;
        default:
            break;
    }
    
    return true;
}
