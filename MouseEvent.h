//
//  MouseEvent.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_MouseEvent_h
#define SimpleChess_MouseEvent_h

#include "Event.h"

class MouseEvent : public Event
{
public:
    MouseEvent(int type, float x, float y) 
    : Event(type)
    , _x(x)
    , _y(y) {};
    ~MouseEvent(void) {};
    
    // mouse events are given in ndc space
    // to make unprojecting easier
    float x(void) const { return _x; };
    float y(void) const { return _y; };
    
private:
    float _x;
    float _y;
    
};

#endif
