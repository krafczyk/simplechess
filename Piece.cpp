//
//  Piece.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Piece.h"

#include "Board.h"
#include "Colors.h"
#include "Model.h"
#include "Shader.h"
#include "Tile.h"

int Piece::_glRefCount = 0;
Shader* Piece::_shader;

Piece::Piece(Color color, Model& model)
: _color(color)
, _model(model)
, _offsetX(0.0f)
, _offsetY(0.0f)
{
    if (_glRefCount == 0)
    {
        _shader = new Shader("SimpleColoredPhong.vs", "SimpleColoredPhong.fs");
    }
    _glRefCount++;
}

Piece::~Piece(void)
{
    _glRefCount--;
    if (_glRefCount == 0)
    {
        delete _shader;
    }
}

// base implementation provides no valid moves
void Piece::getValidMoves(Board& board, std::set<Tile*>& moveList) const
{
    moveList.clear();
}

void Piece::draw(const Screen& screen) const
{
    _shader->use();

    // translate, rotate, and scale the piece model so it's in line with the board
    glTranslatef(0.5f, 0.5f, 0.0f);
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    glScalef(0.05f, 0.05f, 0.05f);
    
    glUniform3fv(_shader->getUniform("color"), 1, (GLfloat*)&((getColor() == BLACK) ? Colors::black : Colors::white));

    _model.draw(screen);
    
    Shader::useDefault();
}
