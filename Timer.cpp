//
//  Timer.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/15/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Timer.h"

#include "Updatable.h"

Timer::Timer(int seedMS)
: _lastMS(seedMS)
{
    
}

Timer::~Timer(void)
{
    
}

void Timer::addListener(Updatable& listener)
{
    _listeners.insert(&listener);
}

void Timer::removeListener(Updatable& listener)
{
    UpdatableContainer::iterator listenerIter = _listeners.find(&listener);
    if (listenerIter != _listeners.end())
    {
        _listeners.erase(listenerIter);
    }
}

void Timer::serviceTimers(int currentMS)
{
    for (UpdatableContainer::iterator listenerIter = _listeners.begin(); listenerIter != _listeners.end(); listenerIter++)
    {
        (*listenerIter)->update(currentMS - _lastMS);
    }
    _lastMS = currentMS;
}
