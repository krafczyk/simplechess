//
//  mat3.h
//
//  Created by Keith Kaisershot on 10/4/13.
//  Copyright (c) 2013 Keith Kaisershot. All rights reserved.
//

#ifndef mat3_h
#define mat3_h

#include <math.h>

#include "pt3.h"
#include "vec2.h"
#include "vec3.h"
#include "mat2.h"

namespace math
{
    template<typename T>
    struct mat3
    {
        T m00;
        T m10;
        T m20;
        T m01;
        T m11;
        T m21;
        T m02;
        T m12;
        T m22;
        
        static mat3<T> make(
            T m00, T m01, T m02,
            T m10, T m11, T m12,
            T m20, T m21, T m22)
        {
            mat3<T> result = {
                m00, m10, m20,
                m01, m11, m21,
                m02, m12, m22
            };
            return result;
        }
        
        const T& operator[](const size_t i) const
        {
            return this->*_elem[i];
        }
        
        T& operator[](const size_t i)
        {
            return this->*_elem[i];
        }
        
        const T& operator()(const size_t row, const size_t col) const
        {
            return this->*_elem[row + (col * 3)];
        }
        
        T& operator()(const size_t row, const size_t col)
        {
            return this->*_elem[row + (col * 3)];
        }
        
        mat3<T> operator+(const mat3<T>& rhs) const
        {
            mat3<T> result = {
                m00 + rhs.m00, m10 + rhs.m10, m20 + rhs.m20,
                m01 + rhs.m01, m11 + rhs.m11, m21 + rhs.m21,
                m02 + rhs.m02, m12 + rhs.m12, m22 + rhs.m22
            };
            return result;
        }
        
        mat3<T> operator-(const mat3<T>& rhs) const
        {
            mat3<T> result = {
                m00 - rhs.m00, m10 - rhs.m10, m20 - rhs.m20,
                m01 - rhs.m01, m11 - rhs.m11, m21 - rhs.m21,
                m02 - rhs.m02, m12 - rhs.m12, m22 - rhs.m22
            };
            return result;
        }
        
        mat3<T> operator*(const T rhs) const
        {
            mat3<T> result = {
                m00 * rhs, m10 * rhs, m20 * rhs,
                m01 * rhs, m11 * rhs, m21 * rhs,
                m02 * rhs, m12 * rhs, m22 * rhs
            };
            return result;
        }
        
        mat3<T> operator/(const T rhs) const
        {
            mat3<T> result = {
                m00 / rhs, m10 / rhs, m20 / rhs,
                m01 / rhs, m11 / rhs, m21 / rhs,
                m02 / rhs, m12 / rhs, m22 / rhs
            };
            return result;
        }
        
        pt3<T> operator*(const pt3<T>& rhs) const
        {
            pt3<T> result = {
                (m00 * rhs.x) + (m01 * rhs.y) + (m02 * rhs.z),
                (m10 * rhs.x) + (m11 * rhs.y) + (m12 * rhs.z),
                (m20 * rhs.x) + (m21 * rhs.y) + (m22 * rhs.z)
            };
            return result;
        }
        
        vec3<T> operator*(const vec3<T>& rhs) const
        {
            vec3<T> result = {
                (m00 * rhs.x) + (m01 * rhs.y) + (m02 * rhs.z),
                (m10 * rhs.x) + (m11 * rhs.y) + (m12 * rhs.z),
                (m20 * rhs.x) + (m21 * rhs.y) + (m22 * rhs.z)
            };
            return result;
        }
        
        mat2<T> minor(const size_t row, const size_t col) const
        {
            mat2<T> result;
            for (size_t i = 0, x = 0; i < 3; ++i)
            {
                if (i != col)
                {
                    for (size_t k = 0, y = 0; k < 3; ++k)
                    {
                        if (k != row)
                        {
                            result(y++, x) = (*this)(k, i);
                        }
                    }
                    ++x;
                }
            }
            return result;
        }
        
        float det() const
        {
            return
                m00 * minor(0, 0).det() -
                m01 * minor(0, 1).det() +
                m02 * minor(0, 2).det();
        }
        
        float cofactor(const size_t row, const size_t col) const
        {
            return minor(row, col).det() * ((int)((row + col) % 2) * -2 + 1);
        }
        
        mat3<T> adj() const
        {
            mat3<T> result = {
                cofactor(0, 0), cofactor(0, 1), cofactor(0, 2),
                cofactor(1, 0), cofactor(1, 1), cofactor(1, 2),
                cofactor(2, 0), cofactor(2, 1), cofactor(2, 2)
            };
            return result;
        }
        
        mat3<T> inv() const
        {
            return adj() / det();
        }
        
        mat3<T> transpose() const
        {
            mat3<T> result = {
                m00, m01, m02,
                m10, m11, m12,
                m20, m21, m22
            };
            return result;
        }
        
        static mat3<T> ident()
        {
            mat3<T> result = {
                1, 0, 0,
                0, 1, 0,
                0, 0, 1
            };
            return result;
        }
        
        static mat3<T> rotX(const float radians)
        {
            float c = cosf(radians);
            float s = sinf(radians);
            mat3<T> result = {
                1, 0, 0,
                0, c, -s,
                0, s, c
            };
            return result;
        }
        
        static mat3<T> rotY(const float radians)
        {
            float c = cosf(radians);
            float s = sinf(radians);
            mat3<T> result = {
                c, 0, s,
                0, 1, 0
                -s, 0, c
            };
            return result;
        }
        
        static mat3<T> scale(const float scale)
        {
            mat3<T> result = {
                scale, 0, 0,
                0, scale, 0,
                0, 0, 1
            };
            return result;
        }
        
        static mat3<T> trans(const vec2<T>& v)
        {
            mat3<T> result = {
                1, 0, 0,
                0, 1, 0,
                v.x, v.y, 1
            };
            return result;
        }
        
        static mat3<T> shearXY(const float s)
        {
            mat3<T> result = {
                1, 0, 0,
                s, 1, 0,
                0, 0, 1
            };
            return result;
        }
        
        static mat3<T> shearYX(const float s)
        {
            mat3<T> result = {
                1, s, 0,
                0, 1, 0,
                0, 0, 1
            };
            return result;
        }
    private:
        static T mat3<T>::*_elem[3 * 3];
    };
    
    template<typename T>
    T mat3<T>::*mat3<T>::_elem[3 * 3] = {
        &mat3<T>::m00, &mat3<T>::m10, &mat3<T>::m20,
        &mat3<T>::m01, &mat3<T>::m11, &mat3<T>::m21,
        &mat3<T>::m02, &mat3<T>::m12, &mat3<T>::m22
    };
    
    typedef mat3<char> mat3c;
    typedef mat3<short> mat3s;
    typedef mat3<int> mat3i;
    typedef mat3<float> mat3f;
}

#endif
