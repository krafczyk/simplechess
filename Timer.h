//
//  Timer.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/15/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Timer_h
#define SimpleChess_Timer_h

#include <set>

class Updatable;

class Timer
{
public:
    // seed with the current time
    Timer(int seedMS);
    ~Timer(void);
    
    void addListener(Updatable& listener);
    void removeListener(Updatable& listener);
    
    // serviceTimers must be called regularly for the
    // time deltas to make much sense
    void serviceTimers(int currentMS);
    
private:
    typedef std::set<Updatable*> UpdatableContainer;
    UpdatableContainer _listeners;
    
    int _lastMS;
    
};

#endif
