//
//  Shader.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/13/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Shader_h
#define SimpleChess_Shader_h

#include "GLEW/glew.h"

class Shader
{
public:
    Shader(const char* vertexPath, const char* fragmentPath);
    ~Shader(void);
    
    unsigned int getUniform(const char* name) const;
    
    void use(void);
    static void useDefault(void);
    
private:
    GLuint _program;
    
    char* readShaderFile(const char* shaderPath);

};

#endif
