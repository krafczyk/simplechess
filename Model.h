//
//  Model.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/14/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Model_h
#define SimpleChess_Model_h

#include <vector>

#include "GLEW/glew.h"

#include "Drawable.h"

class Model : public Drawable
{
public:
    // objPath is relative to the data directory
    // shapeName is the name of an object/group in the obj file
    // empty string if there are no objects/groups defined
    Model(const char* objPath, const char* shapeName);
    ~Model(void);
    
    void draw(const Screen& screen) const;
    
private:
    typedef std::vector<float> PositionContainer;
    PositionContainer _positions;
    typedef std::vector<float> TexCoordContainer;
    TexCoordContainer _texCoords;
    typedef std::vector<float> NormalContainer;
    NormalContainer _normals;
    typedef std::vector<unsigned short> IndexContainer;
    IndexContainer _indices;
    
    GLuint _vao;
    GLuint _vbo;
    GLuint _vboi;
    long _positionsByteSize;
    long _texCoordsByteSize;
    long _normalsByteSize;
    
};

#endif
