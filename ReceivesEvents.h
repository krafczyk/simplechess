//
//  ReceivesEvents.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/12/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_ReceivesEvents_h
#define SimpleChess_ReceivesEvents_h

class Event;

class ReceivesEvents
{
public:
    /* receiveEvent takes an Event object with a type recognized by the child class
     and responds to it, returning true if the event should continue to remaining
     receivers or false otherwise.
     */
    virtual bool receiveEvent(Event& event) = 0;
    
};

#endif
