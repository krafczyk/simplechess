#version 120

attribute vec3 position;
attribute vec3 normal;
varying vec3 P;
varying vec3 N;
varying vec3 V;

void main()
{
    vec4 position4 = vec4(position, 1.0);
    P = (gl_ModelViewMatrix * position4).xyz;
    N = normalize(gl_NormalMatrix * normal);
    V = normalize(-P);
    
	gl_Position =  gl_ModelViewProjectionMatrix * vec4(position, 1.0);
}
