//
//  Model.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/14/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Model.h"

#include "tiny_obj_loader.h"

#include "Logging.h"

Model::Model(const char* objPath, const char* shapeName)
{
    std::vector<tinyobj::shape_t> shapes;
    std::string fullObjPath = std::string(SDL_GetBasePath()) + objPath;
    tinyobj::LoadObj(shapes, fullObjPath.c_str());
    
    for (std::vector<tinyobj::shape_t>::iterator shapeIter = shapes.begin(); shapeIter != shapes.end(); shapeIter++)
    {
        if (shapeIter->name == shapeName)
        {
            _positions = shapeIter->mesh.positions;
            _texCoords = shapeIter->mesh.texcoords;
            _normals = shapeIter->mesh.normals;
            _indices.reserve(shapeIter->mesh.indices.size());
            
            for (std::vector<unsigned int>::iterator indexIter = shapeIter->mesh.indices.begin(); indexIter != shapeIter->mesh.indices.end(); indexIter++)
            {
                // store the indices as unsigned shorts because they're smaller
                _indices.push_back(*indexIter);
            }
            
            _positionsByteSize = _positions.size() * sizeof(_positions[0]);
            _texCoordsByteSize = _texCoords.size() * sizeof(_texCoords[0]);
            _normalsByteSize = _normals.size() * sizeof(_normals[0]);
            long indicesByteSize = _indices.size() * sizeof(_indices[0]);

            glGenBuffers(1, &_vbo);
            glBindBuffer(GL_ARRAY_BUFFER, _vbo);
            glBufferData(GL_ARRAY_BUFFER, _positionsByteSize + _texCoordsByteSize + _normalsByteSize, NULL, GL_STATIC_DRAW);
            glBufferSubData(GL_ARRAY_BUFFER, 0, _positionsByteSize, &_positions[0]);
            glBufferSubData(GL_ARRAY_BUFFER, _positionsByteSize, _texCoordsByteSize, &_texCoords[0]);
            glBufferSubData(GL_ARRAY_BUFFER, _positionsByteSize + _texCoordsByteSize, _normalsByteSize, &_normals[0]);
            
            glGenBuffers(1, &_vboi);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vboi);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesByteSize, &_indices[0], GL_STATIC_DRAW);
            
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            break;
        }
    }
}

Model::~Model(void)
{
    glDeleteBuffers(1, &_vboi);
    glDeleteBuffers(1, &_vbo);
}

void Model::draw(const Screen& screen) const
{    
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vboi);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(_positionsByteSize + _texCoordsByteSize));
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vboi);
    glDrawElements(GL_TRIANGLES, (GLint)_indices.size(), GL_UNSIGNED_SHORT, 0);
    
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);    
}
