//
//  Sound.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Sound_h
#define SimpleChess_Sound_h

#include "SDL2_mixer/SDL_mixer.h"

class Sound
{
public:
    // soundPath is relative to the data directory
    Sound(const char* soundPath);
    ~Sound(void);
    
    void play(void) const;
    
private:
    Mix_Chunk* _sound;
    
};

#endif
