//
//  Sound.cpp
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#include "Sound.h"

#include "Logging.h"

Sound::Sound(const char* soundPath)
{
    _sound = Mix_LoadWAV(soundPath);
    if (_sound == NULL)
    {
        Logging::error("Could not load sound: %s\n", Mix_GetError());
    }
}

Sound::~Sound(void)
{
    if (_sound != NULL)
    {
        Mix_FreeChunk(_sound);
    }
}

void Sound::play(void) const
{
    if (_sound != NULL)
    {
        Mix_PlayChannel(-1, _sound, 0);
    }
}
