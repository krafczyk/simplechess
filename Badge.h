//
//  Badge.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/16/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Badge_h
#define SimpleChess_Badge_h

#include "GLEW/glew.h"
#include "SDL2_ttf/SDL_ttf.h"

#include "Drawable.h"
#include "rgb.h"

class Screen;
class Shader;

using namespace geom;

class Badge : public Drawable
{
public:
    Badge(int fontSize, rgbf color);
    ~Badge(void);
    
    static bool isInited(void) { return _inited; };

    void updateText(const char* text);
    void setAlphaFactor(float alphaFactor);
    
    int getWidth(void) const { return _width; };
    int getHeight(void) const { return _height; };
    
    float getMaxU(void) const { return _maxU; };
    float getMaxV(void) const { return _maxV; };
    
    void draw(const Screen& screen) const;
    
private:
    int _fontSize;
    TTF_Font* _font;
    rgbf _color;
    float _alphaFactor;
    
    GLuint _texture;
    int _width, _height;
    GLfloat _maxU, _maxV;
    
    // we only need one vbo and shader for all badges
    // so keep a reference count of all badges
    // and clean up the gl stuff when all badges are gone
    static bool _inited;
    static int _glRefCount;
    static Shader* _shader;
    static GLuint _vbo;
    
    void shutdown(void);

};

#endif
