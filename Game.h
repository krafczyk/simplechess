//
//  Game.h
//  SimpleChess
//
//  Created by Keith Kaisershot on 5/12/14.
//  Copyright (c) 2014 Keith Kaisershot. All rights reserved.
//

#ifndef SimpleChess_Game_h
#define SimpleChess_Game_h

#include <vector>

#include "SDL2/SDL.h"

#include "Board.h"
#include "HUD.h"
#include "Piece.h"
#include "ReceivesEvents.h"

class Event;
class EventManager;
class Screen;
class Sound;
class SoundManager;
class Timer;

class Game : public ReceivesEvents
{
public:
    Game(void);
    ~Game(void);
    
    bool isInited(void) const { return _inited; };
    bool isRunning(void) const { return _running; };
        
    void reset(void);
    
    bool handleInput(void);
    void processFrame(void);
    
    bool receiveEvent(Event& event);
    
private:
    bool _inited;
    bool _running;
    EventManager* _eventManager;
    SoundManager* _soundManager;
    Screen* _screen;
    Timer* _timer;
    
    HUD* _hud;
    Board* _board;
    Model* _pieceModels[3];
    Sound* _captureSound;
    typedef std::vector<Piece*> PieceContainer;
    PieceContainer _pieces;
    
    Piece::Color _currentPlayer;
    
    Tile* _interestingTile;
    typedef std::set<Tile*> ValidMoveContainer;
    ValidMoveContainer _hintTiles;
    
    Tile* _selectedTile;
    
    int _scores[Piece::NUM_COLORS];
    
    void shutdown(void);
    
};

#endif
